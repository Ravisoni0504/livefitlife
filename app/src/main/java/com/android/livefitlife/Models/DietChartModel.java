package com.android.livefitlife.Models;

public class DietChartModel {

    String dietName;
    String levelName;
    String dietChartPath;
    String levelId;

    public String getDietName() {
        return dietName;
    }

    public void setDietName(String dietName) {
        this.dietName = dietName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getDietChartPath() {
        return dietChartPath;
    }

    public void setDietChartPath(String dietChartPath) {
        this.dietChartPath = dietChartPath;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }
}
