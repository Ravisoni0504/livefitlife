package com.android.livefitlife.Models;

public class ActivityModel {

    String activityId;

    String activityName;

    String activityIcon;
    String is_lfl;

    public String getIs_lfl() {
        return is_lfl;
    }

    public void setIs_lfl(String is_lfl) {
        this.is_lfl = is_lfl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    String description;

    boolean isVisible = true;

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityIcon() {
        return activityIcon;
    }

    public void setActivityIcon(String activityIcon) {
        this.activityIcon = activityIcon;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String id) {
        this.activityId = id;
    }
}
