package com.android.livefitlife.Models;

public class MySubscriptionModel {

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_category() {
        return plan_category;
    }

    public void setPlan_category(String plan_category) {
        this.plan_category = plan_category;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    String plan_id;
    String plan_category;
    String plan_name;
    String start_date;
    String end_date;
    String price;

    public String getPlan_days() {
        return plan_days;
    }

    public void setPlan_days(String plan_days) {
        this.plan_days = plan_days;
    }

    String plan_days;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;
}
