package com.android.livefitlife.Models;

public class TodayTaskModel {

    String trainerName;
    String trainerPhoto;
    String trainerCategory;

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    String roomName;

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }

    public String getTrainerPhoto() {
        return trainerPhoto;
    }

    public void setTrainerPhoto(String trainerPhoto) {
        this.trainerPhoto = trainerPhoto;
    }

    public String getTrainerCategory() {
        return trainerCategory;
    }

    public void setTrainerCategory(String trainerCategory) {
        this.trainerCategory = trainerCategory;
    }

    public String getTrainer_id() {
        return trainer_id;
    }

    public void setTrainer_id(String trainer_id) {
        this.trainer_id = trainer_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String trainer_id;
    String time;
}
