package com.android.livefitlife.Models;

public class LevelModel {

    String levelName;
    String levelId;
    String isUnlocked;


    public String getIsUnlocked() {
        return isUnlocked;
    }

    public void setIsUnlocked(String isUnlocked) {
        this.isUnlocked = isUnlocked;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }
}
