package com.android.livefitlife.Models;

public class DietRecipeModel {

    String nametitle;
    String idDietReceipe;
    String youtubeLink;

    public String getNametitle() {
        return nametitle;
    }

    public void setNametitle(String nametitle) {
        this.nametitle = nametitle;
    }

    public String getIdDietReceipe() {
        return idDietReceipe;
    }

    public void setIdDietReceipe(String idDietReceipe) {
        this.idDietReceipe = idDietReceipe;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }
}
