package com.android.livefitlife.Models;

public class WorkoutModel {

    int imageSet;

    String exerciseName;

    int likeIMage;

    int cardColor;

    String exerciseTime;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String image;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    String description;


    public int getImageSet() {
        return imageSet;
    }

    public void setImageSet(int imageSet) {
        this.imageSet = imageSet;
    }

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public String getExerciseTime() {
        return exerciseTime;
    }

    public void setExerciseTime(String exerciseTime) {
        this.exerciseTime = exerciseTime;
    }

    public int getLikeIMage() {
        return likeIMage;
    }

    public void setLikeIMage(int likeIMage) {
        this.likeIMage = likeIMage;
    }

    public int getCardColor() {
        return cardColor;
    }

    public void setCardColor(int cardColor) {
        this.cardColor = cardColor;
    }
}
