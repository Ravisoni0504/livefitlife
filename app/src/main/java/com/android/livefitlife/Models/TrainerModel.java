package com.android.livefitlife.Models;

public class TrainerModel {

    String trainerName;
    String trainerId;
    String trainerImage;
    String trainerRating;
    String trainerExperince;
    String trainerExpertise;

    public String getTrainerRating() {
        return trainerRating;
    }

    public void setTrainerRating(String trainerRating) {
        this.trainerRating = trainerRating;
    }

    public String getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(String trainerId) {
        this.trainerId = trainerId;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }

    public String getTrainerImage() {
        return trainerImage;
    }

    public void setTrainerImage(String trainerImage) {
        this.trainerImage = trainerImage;
    }

    public String getTrainerExperince() {
        return trainerExperince;
    }

    public void setTrainerExperince(String trainerExperince) {
        this.trainerExperince = trainerExperince;
    }

    public String getTrainerExpertise() {
        return trainerExpertise;
    }

    public void setTrainerExpertise(String trainerExpertise) {
        this.trainerExpertise = trainerExpertise;
    }
}
