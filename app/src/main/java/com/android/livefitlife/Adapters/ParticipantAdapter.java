package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.R;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import java.util.List;

public class ParticipantAdapter extends RecyclerView.Adapter<ParticipantAdapter.MyViewHolder> {

    List<VideoTrack> listData;
    Context context;
  //  OnClickActivity onClickDietPlan;


    public ParticipantAdapter(Context context, List<VideoTrack> list) {
        this.listData = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.addnewparticipant_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        holder.videoViewParticipant.setMirror(false);
        listData.get(position).addRenderer(holder.videoViewParticipant);

        //   holder.activityName.setText(listData.get(position).getActivityName());
        //   Picasso.get().load(listData.get(position).getActivityIcon()).into(holder.activityImage);

        //  if (!listData.get(position).isVisible()) {

        // }

        //   holder.itemView.setOnClickListener(new View.OnClickListener() {
        //     @Override
        //   public void onClick(View v) {
        // onClickDietPlan.OnClickActivity(position);
        //     }
        //});
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView userName;
        VideoView videoViewParticipant;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            videoViewParticipant = itemView.findViewById(R.id.participantView);
            userName = itemView.findViewById(R.id.userName);

        }
    }

}
