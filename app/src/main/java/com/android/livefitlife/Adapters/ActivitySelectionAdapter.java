package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ActivitySelectionAdapter extends RecyclerView.Adapter<ActivitySelectionAdapter.MyViewHolder> {

    List<ActivityModel> listData;
    Context context;
    OnClickActivity onClickDietPlan;


    public ActivitySelectionAdapter(Context context, List<ActivityModel> list, OnClickActivity onClickDietPlan) {
        this.listData = list;
        this.onClickDietPlan = onClickDietPlan;
        this.context = context;
    }

    public ActivitySelectionAdapter(Context context, List<ActivityModel> list) {
        this.listData = list;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        holder.activityName.setText(listData.get(position).getActivityName());
        Picasso.get().load(listData.get(position).getActivityIcon()).into(holder.activityImage);
        //    holder.activityImage.setImageResource(listData.get(position).getActivityIcon());
        if (!listData.get(position).isVisible()) {

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDietPlan.OnClickActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView activityName;
        ImageView activityImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            activityImage = itemView.findViewById(R.id.activityImage);
            activityName = itemView.findViewById(R.id.activityName);

        }
    }

}
