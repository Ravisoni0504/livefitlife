package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.Models.MySubscriptionModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnClickActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MySubscriptionAdapter extends RecyclerView.Adapter<MySubscriptionAdapter.MyViewHolder> {

    List<MySubscriptionModel> listData;
    Context context;
    OnClickActivity onClickDietPlan;


    public MySubscriptionAdapter(Context context, List<MySubscriptionModel> list) {
        this.listData = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mysubscriptions_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.category_name.setText(listData.get(position).getPlan_category());
        holder.pack_name.setText(listData.get(position).getPlan_name());
        holder.start_date.setText("Date of Purchase " + listData.get(position).getStart_date());
        holder.end_date.setText(listData.get(position).getEnd_date());
        holder.status.setText(listData.get(position).getStatus());
        holder.price.setText("INR " + listData.get(position).getPrice());
        holder.plan_days.setText("For " + listData.get(position).getPlan_days()+" days");

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView category_name, price, start_date, end_date, status, pack_name, plan_days;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            category_name = itemView.findViewById(R.id.categoryName);
            price = itemView.findViewById(R.id.price);
            start_date = itemView.findViewById(R.id.startDate);
            end_date = itemView.findViewById(R.id.endDate);
            status = itemView.findViewById(R.id.status);
            pack_name = itemView.findViewById(R.id.pack_name);
            plan_days = itemView.findViewById(R.id.plan_days);

        }
    }

}
