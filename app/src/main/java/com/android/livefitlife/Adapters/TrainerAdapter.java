package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.TrainerModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnClickDietPlan;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TrainerAdapter extends RecyclerView.Adapter<TrainerAdapter.MyViewHolder> {

    List<TrainerModel> professionalListModels;
    Context context;
    OnClickDietPlan onClickDietPlan;


    public TrainerAdapter(Context context, List<TrainerModel> professionalListModels1, OnClickDietPlan onClickDietPlan) {
        this.professionalListModels = professionalListModels1;
        this.onClickDietPlan = onClickDietPlan;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trainer_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        TrainerModel trainerModel = professionalListModels.get(position);

        String category = trainerModel.getTrainerExpertise().replace(",", "   |   ");

        holder.expertise.setText(category);
        holder.userName.setText(trainerModel.getTrainerName());
        holder.ratingBar.setRating(Float.parseFloat(trainerModel.getTrainerRating()));
        holder.experience.setText(trainerModel.getTrainerExperince());
        Picasso.get().load(trainerModel.getTrainerImage()).into(holder.image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDietPlan.OnClickDiet(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return professionalListModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView userName, experience, expertise;
        ImageView image;
        RatingBar ratingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            experience = itemView.findViewById(R.id.experience);
            userName = itemView.findViewById(R.id.userName);
            expertise = itemView.findViewById(R.id.expertise);
            image = itemView.findViewById(R.id.image);
            ratingBar = itemView.findViewById(R.id.ratingBar);

        }
    }

}
