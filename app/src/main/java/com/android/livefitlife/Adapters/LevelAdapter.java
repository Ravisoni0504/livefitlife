package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.LevelModel;
import com.android.livefitlife.R;

import java.util.List;

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.MyViewHolder> {

    List<LevelModel> levelModelList;
    Context context;


    public LevelAdapter(Context context, List<LevelModel> levelModelList) {
        this.levelModelList = levelModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return levelModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, address, rating_text, totalrating;
        ImageView image;
        RatingBar ratingBar;
        RelativeLayout categorymain;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
