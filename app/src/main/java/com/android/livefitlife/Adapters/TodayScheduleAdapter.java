package com.android.livefitlife.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.Models.TodayTaskModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.VideoCalling.VideoActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TodayScheduleAdapter extends RecyclerView.Adapter<TodayScheduleAdapter.MyViewHolder> {

    List<TodayTaskModel> listData;
    Context context;
    OnClickActivity onClickActivity;


    public TodayScheduleAdapter(Context context, List<TodayTaskModel> list,OnClickActivity clickActivity) {
        this.listData = list;
        this.onClickActivity=clickActivity;
        this.context = context;
    }

    @NonNull
    @Override
    public TodayScheduleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_task_layout, parent, false);
        return new TodayScheduleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TodayScheduleAdapter.MyViewHolder holder, final int position) {


        holder.trainerName.setText(listData.get(position).getTrainerName());
        holder.trainerCat.setText(listData.get(position).getTrainerCategory());
        holder.time.setText(listData.get(position).getTime());
        Picasso.get().load(listData.get(position).getTrainerPhoto()).placeholder(R.drawable.defaultt).into(holder.trainerImage);


        holder.joinClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onClickActivity.OnClickActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView trainerName, trainerCat, time;
        ImageView trainerImage;
        RelativeLayout joinClass;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            trainerImage = itemView.findViewById(R.id.image);
            trainerName = itemView.findViewById(R.id.trainerName);
            trainerCat = itemView.findViewById(R.id.trainerAct);
            time = itemView.findViewById(R.id.dateEvent);
            joinClass = itemView.findViewById(R.id.joinClass);

        }
    }
}
