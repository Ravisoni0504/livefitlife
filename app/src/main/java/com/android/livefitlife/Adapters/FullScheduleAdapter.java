package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.FullScheduleModel;
import com.android.livefitlife.R;

import java.util.List;

public class FullScheduleAdapter extends RecyclerView.Adapter<FullScheduleAdapter.MyViewHolder> {

    List<FullScheduleModel> listData;
    Context context;

    public FullScheduleAdapter(Context context, List<FullScheduleModel> list) {
        this.listData = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.full_schedule_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        //  holder.activityName.setText(listData.get(position).getActivityName());

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView activityName;
        ImageView activityImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            //  activityImage = itemView.findViewById(R.id.profile);
            //activityName = itemView.findViewById(R.id.text);

        }
    }

}
