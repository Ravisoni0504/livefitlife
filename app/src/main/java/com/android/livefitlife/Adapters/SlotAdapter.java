package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.SlotModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnSlotClick;

import java.util.ArrayList;

public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.ViewHolder> {

    private ArrayList<SlotModel> arrayList;
    private LayoutInflater mInflater;
    OnSlotClick onSlotClick;
    Context context;

    // data is passed into the constructor
    public SlotAdapter(Context context, ArrayList<SlotModel> data, OnSlotClick onSlotClick) {
        this.mInflater = LayoutInflater.from(context);
        this.arrayList = data;
        this.onSlotClick = onSlotClick;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.slot_item, parent, false);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (arrayList.get(position).isAvailable()) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.orangetheme));
            holder.myTextView.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            holder.myTextView.setTextColor(context.getResources().getColor(R.color.orangetheme));
        }

        holder.myTextView.setText(arrayList.get(position).getTime());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reselectAll();

                notifyDataSetChanged();
                onSlotClick.onSlotTap(position);

                arrayList.get(position).setAvailable(true);
                notifyDataSetChanged();

            }
        });
    }

    public void reselectAll() {

        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.get(i).setAvailable(false);
        }


    }

    // total number of cells
    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    @Override
    public long getItemId(int position) {
        return arrayList.get(position).getId().hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);

            myTextView = itemView.findViewById(R.id.titleService);
            cardView = itemView.findViewById(R.id.cardView);

        }
    }

}
