package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.LevelModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnClickActivity;

import java.util.List;

public class LevelListAdapter extends RecyclerView.Adapter<LevelListAdapter.MyViewHolder> {

    List<LevelModel> listData;
    Context context;
    OnClickActivity onClickDietPlan;


    public LevelListAdapter(Context context, List<LevelModel> list, OnClickActivity onClickDietPlan) {
        this.listData = list;
        this.onClickDietPlan = onClickDietPlan;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.level_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.levelName.setText(listData.get(position).getLevelName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDietPlan.OnClickActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView levelName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            levelName = itemView.findViewById(R.id.levelName);

        }
    }

}
