package com.android.livefitlife.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Activities.PlayYoutubeActivity;
import com.android.livefitlife.Models.DietRecipeModel;
import com.android.livefitlife.R;

import java.util.List;

public class VideoListDietAdapter extends RecyclerView.Adapter<VideoListDietAdapter.MyViewHolder> {

    List<DietRecipeModel> professionalListModels;
    Context context;

    public VideoListDietAdapter(Context context, List<DietRecipeModel> professionalListModels1) {
        this.professionalListModels = professionalListModels1;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.diet_recipe_xml, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final DietRecipeModel dietRecipeModel = professionalListModels.get(position);
        holder.titleFruitSalad.setText(dietRecipeModel.getNametitle());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String playerId = dietRecipeModel.getYoutubeLink();

                String[] playerIdArry;
                String code = "";

                if (playerId.contains("=")) {

                    playerIdArry = playerId.split("=");
                    code = playerIdArry[1];

                } else {

                    playerIdArry = playerId.split("/");
                    code = playerIdArry[playerIdArry.length - 1];

                }

                Intent intent = new Intent(context, PlayYoutubeActivity.class);
                intent.putExtra("playerId", code);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return professionalListModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView titleFruitSalad;
        ImageView image;

        public MyViewHolder(@NonNull View itemView) {

            super(itemView);

            titleFruitSalad = itemView.findViewById(R.id.titleFruitSalad);

        }
    }

}
