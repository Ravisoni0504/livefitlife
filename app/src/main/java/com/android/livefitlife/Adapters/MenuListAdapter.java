package com.android.livefitlife.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Activities.BookSlotActivity;
import com.android.livefitlife.Activities.ContactUsActivity;
import com.android.livefitlife.Activities.LeaderBoardActivity;
import com.android.livefitlife.Activities.LoginActivity;
import com.android.livefitlife.Activities.MySubscription;
import com.android.livefitlife.Activities.NavDashActivity;
import com.android.livefitlife.Models.MenuModelMain;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.facebook.login.LoginManager;

import java.util.List;

import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_USER_ID;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.MyViewHolder> {

    List<MenuModelMain> professionalListModels;
    Context context;


    public MenuListAdapter(Context context, List<MenuModelMain> professionalListModels1) {
        this.professionalListModels = professionalListModels1;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        MenuModelMain professionalListModel = professionalListModels.get(position);

        holder.home_menu.setText(professionalListModel.getMenuName());
        holder.menu_icon.setImageResource(professionalListModel.getMenuIcon());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent in = new Intent(context, NavDashActivity.class);
                    context.startActivity(in);

                } else if (position == 1) {

                    Intent intent = new Intent(context, MySubscription.class);
                    context.startActivity(intent);

                } else if (position == 2) {

                    Intent intent = new Intent(context, LeaderBoardActivity.class);
                    context.startActivity(intent);

                }else if (position == 3) {

                    Intent in = new Intent(context, ContactUsActivity.class);
                    context.startActivity(in);

                } else if (position == 4) {

                    SharedpreferenceUtility.getInstance(context).setUser(SAVE_USER_ID, "0");
                    SharedpreferenceUtility.getInstance(context).setUser(SAVE_TOKEN, "0");
                    LoginManager.getInstance().logOut();
                    Intent in = new Intent(context, LoginActivity.class);
                    context.startActivity(in);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return professionalListModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView home_menu;
        ImageView menu_icon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            menu_icon = itemView.findViewById(R.id.menu_icon);
            home_menu = itemView.findViewById(R.id.home_menu);

        }
    }

}
