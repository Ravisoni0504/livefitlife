package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.UserModel;
import com.android.livefitlife.R;

import java.util.List;

public class UserLevelListCardAdapter extends RecyclerView.Adapter<UserLevelListCardAdapter.MyViewHolder> {

    List<UserModel> professionalListModels;
    Context context;


    public UserLevelListCardAdapter(Context context, List<UserModel> professionalListModels1) {
        this.professionalListModels = professionalListModels1;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_level_list_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

//        final UserModel professionalListModel = professionalListModels.get(position);

      //  holder.name.setText(professionalListModel.getName());
        //   holder.address.setText(professionalListModel.getAddress());
        //  Picasso.get().load(BASE_URL + professionalListModel.getImage()).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, address, rating_text, totalrating;
        ImageView image;
        RatingBar ratingBar;
        RelativeLayout categorymain;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            //  name = itemView.findViewById(R.id.profession_name);

        }
    }

}
