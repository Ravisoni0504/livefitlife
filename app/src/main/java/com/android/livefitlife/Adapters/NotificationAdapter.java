package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.NotificationModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnClickDietPlan;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    List<NotificationModel> notificationModels;
    Context context;
    OnClickDietPlan onClickDietPlan;


    public NotificationAdapter(Context context, List<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.title.setText(notificationModels.get(position).getTitle());//"Notification " + getIndexText(position));
        holder.message.setText(notificationModels.get(position).getDescription());
        holder.date.setText(notificationModels.get(position).getTimeStamp());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //onClickDietPlan.OnClickDiet(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();  //5

    }

    public String getIndexText(int pos) {
        String textStr = "";
        if (pos == 0) {
            textStr = "One";
        } else if (pos == 1) {
            textStr = "Two";
        } else if (pos == 2) {
            textStr = "Three";
        } else if (pos == 3) {
            textStr = "Four";
        } else if (pos == 4) {
            textStr = "Five";
        }

        return textStr;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, message, date;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message);
            title = itemView.findViewById(R.id.title);
            date = itemView.findViewById(R.id.date);


        }
    }

}
