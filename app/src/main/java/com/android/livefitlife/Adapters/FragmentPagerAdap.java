package com.android.livefitlife.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.android.livefitlife.Fragments.DietFrag;
import com.android.livefitlife.Fragments.MemberShipFrag;
import com.android.livefitlife.Fragments.ProfileFrag;
import com.android.livefitlife.Fragments.ProgressFrag;
import com.android.livefitlife.Fragments.WorkoutFrag;

public class FragmentPagerAdap extends FragmentPagerAdapter {

    public FragmentPagerAdap(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {

            case 0:
                return new ProgressFrag();
            case 1:
                return new DietFrag();
            case 2:
                return new WorkoutFrag();
            case 3:
                return new MemberShipFrag();
            case 4:
                return new ProfileFrag();
            default:
                return new WorkoutFrag();
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
