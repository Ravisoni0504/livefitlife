package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.WorkoutModel;
import com.android.livefitlife.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class WorkoutListAdapter extends RecyclerView.Adapter<WorkoutListAdapter.MyViewHolder> {

    List<WorkoutModel> list;
    Context context;

    public WorkoutListAdapter(Context context, List<WorkoutModel> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.workout_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.mainChart.setCardBackgroundColor(list.get(position).getCardColor());
        holder.exerciseName.setText(list.get(position).getExerciseName());

        String desc = android.text.Html.fromHtml(list.get(position).getDescription()).toString();
        holder.exerciseSets.setText(desc);
        //   holder.exercseImage.setImageResource(list.get(position).getImageSet());
        //   holder.likeImage.setImageResource(list.get(position).getLikeIMage());

        Glide.with(context).asGif().load(list.get(position).getImage()).into(holder.exercseImage);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView exerciseName, exerciseSets;
        ImageView exercseImage, likeImage;

        CardView mainChart;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            likeImage = itemView.findViewById(R.id.likeImage);
            exerciseName = itemView.findViewById(R.id.exerciseName);
            exerciseSets = itemView.findViewById(R.id.exerciseSets);
            exercseImage = itemView.findViewById(R.id.exercseImage);
            mainChart = itemView.findViewById(R.id.mainChart);
        }
    }

}
