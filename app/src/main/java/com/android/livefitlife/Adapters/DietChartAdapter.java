package com.android.livefitlife.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Models.DietChartModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.OnClickDietPlan;

import java.util.List;

public class DietChartAdapter extends RecyclerView.Adapter<DietChartAdapter.MyViewHolder> {

    List<DietChartModel> professionalListModels;
    Context context;
    OnClickDietPlan onClickDietPlan;


    public DietChartAdapter(Context context, List<DietChartModel> professionalListModels1, OnClickDietPlan onClickDietPlan) {
        this.professionalListModels = professionalListModels1;
        this.onClickDietPlan = onClickDietPlan;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.diet_cahrt_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.dietChartName.setText(professionalListModels.get(position).getDietName());
        holder.tvLevel.setText(professionalListModels.get(position).getLevelName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDietPlan.OnClickDiet(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return professionalListModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvLevel, dietChartName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            dietChartName = itemView.findViewById(R.id.dietChartName);
            tvLevel = itemView.findViewById(R.id.tvLevel);

        }
    }

}
