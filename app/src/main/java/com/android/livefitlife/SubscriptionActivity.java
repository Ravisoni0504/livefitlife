package com.android.livefitlife;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Activities.BookSlotActivity;
import com.android.livefitlife.Adapters.ActivitySelectionAdapter;
import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.OnClickBuyPlan;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.vivekkaushik.datepicker.DatePickerTimeline;
import com.vivekkaushik.datepicker.OnDateSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_EMAIL;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_PHONE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class SubscriptionActivity extends BaseActivity implements PaymentResultWithDataListener /*implements PaymentResultListener*/ {
    RecyclerView listOfSubscrption;
    ArrayList<SubscriptionModel> arrayList;
    String categoryId, categoryName, trainingType = "", diet_typecount = "", razor_id = "", order_id = "";
    private static final String TAG = SubscriptionActivity.class.getSimpleName();
    final Checkout co = new Checkout();
    private List<ActivityModel> listActivity;
    RecyclerView activityList;
    DatePickerTimeline datePickerTimeline;
    ActivitySelectionAdapter activitySelectionAdapter;
    String levelId, time, date, slotTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription);

        Checkout.preload(getApplicationContext());

        categoryId = getIntent().getStringExtra("categoryID");
        trainingType = getIntent().getStringExtra("trainingType");

        ImageView back_button = findViewById(R.id.back_button);
        Button requestDemo = findViewById(R.id.requestDemo);

        listOfSubscrption = findViewById(R.id.listOfSubscrption);
        listOfSubscrption.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        if (trainingType.equals("VirtualTraining")) {
            requestDemo.setVisibility(View.VISIBLE);
        } else {
            requestDemo.setVisibility(View.GONE);
        }

        requestDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDemoOrTest();
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getPlanDetails();

    }

    public void showDialogDemoOrTest() {
        View view = getLayoutInflater().inflate(R.layout.dialogrequestdemo, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        Button submit = dialog.findViewById(R.id.submitQuery);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (date.equals("null") && time.equals("null")) {
                    Toast.makeText(getApplicationContext(), "Select date and time for demo request", Toast.LENGTH_SHORT).show();
                } else {
                    postRequestDemo(dialog);
                }
            }
        });

        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        datePickerTimeline = dialog.findViewById(R.id.datePickerTimeline);

        activityList = dialog.findViewById(R.id.activityList);
        activityList.setLayoutManager(new LinearLayoutManager(this));

        getActivitiesList();

        final LinearLayout timeCard = dialog.findViewById(R.id.timeCard);

        timeCard.setVisibility(View.GONE);

        final TextView hourTv = dialog.findViewById(R.id.hourTv);
        final TextView minuteTv = dialog.findViewById(R.id.minuteTv);
        final TextView am_time = dialog.findViewById(R.id.am_time);
        final TextView pm_time = dialog.findViewById(R.id.pm_time);

        // Set a Start date (Default, 1 Jan 1970)
        datePickerTimeline.setInitialDate(2020, 10, 25);
        datePickerTimeline.setDateTextColor(getResources().getColor(R.color.orangetheme));
        datePickerTimeline.setDayTextColor(getResources().getColor(R.color.orangetheme));
        datePickerTimeline.setMonthTextColor(getResources().getColor(R.color.orangethemeTra));

        // Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                // Do Something
                onDateSelection(hourTv, minuteTv, am_time, pm_time, timeCard);

                date = String.valueOf(year) + "-" + (month + 1) + "-" + day;

                Log.e("Date", String.valueOf(year) + (month + 1) + day);
            }

            @Override
            public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                // Do Something
            }
        });

        datePickerTimeline.setVisibility(View.GONE);
    }

    public void onDateSelection(final TextView hourTV, final TextView minuteTV, final TextView am_time, final TextView pm_time, final LinearLayout timeCard) {
        Calendar mcurrentTime = Calendar.getInstance();

        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(SubscriptionActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                timeCard.setVisibility(View.VISIBLE);

                hourTV.setText(selectedHour + "");
                minuteTV.setText(selectedMinute + "");

                time = selectedHour + ":" + selectedMinute;

                if (selectedHour > 12) {

                    am_time.setText("PM");
                    pm_time.setText("AM");

                } else {

                    am_time.setText("AM");
                    pm_time.setText("PM");
                }

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void getActivitiesList() {

        showProgressDialog();
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            listActivity = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                ActivityModel activityModel = new ActivityModel();
                                activityModel.setActivityId(dataObject.getString("id"));
                                activityModel.setIs_lfl(dataObject.getString("is_lfl"));
                                activityModel.setDescription(dataObject.getString("description"));
                                activityModel.setActivityName(dataObject.getString("name"));
                                activityModel.setActivityIcon(dataObject.getString("image"));

                                listActivity.add(activityModel);

                            }

                            activitySelectionAdapter = new ActivitySelectionAdapter(getApplicationContext(), listActivity, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {

                                    levelId = listActivity.get(position).getActivityId();

                                    datePickerTimeline.setVisibility(View.VISIBLE);

                                    ActivityModel activityModel = listActivity.get(position);

                                    ArrayList<ActivityModel> arrayList = new ArrayList<>();
                                    arrayList.add(activityModel);

                                    ActivitySelectionAdapter adapter = new ActivitySelectionAdapter(SubscriptionActivity.this, arrayList);
                                    activityList.setAdapter(adapter);

                                }
                            });

                            activityList.setAdapter(activitySelectionAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(SubscriptionActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(SubscriptionActivity.this).addTorequestque(jsonObjReq);

    }


    public void postRequestDemo(Dialog dialog) {

        showProgressDialog();

        slotTime = date + " " + time;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("category_id", levelId);
            jsonObject.put("description", "desc");
            jsonObject.put("slot_time", slotTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.POST_REQUEST_DEMO, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        Log.e("dataPlan", response.toString());
                        try {

                            String status = response.getString("status");
                            JSONObject data = response.getJSONObject("data");

                            if (status.equals("1")) {
                                Toast.makeText(getApplicationContext(), "Your demo request has been received.\nTrainer will be assigned shortly"
                                        , Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        } catch (Exception e) {
                            Log.e("dataPlanException", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(SubscriptionActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(SubscriptionActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };
        Mysingleton.getInstance(SubscriptionActivity.this).addTorequestque(jsonObjReq);
    }


    public void startPayment(String razor_id, int price) {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;
        //    final Checkout co = new Checkout();
        try {
            co.setFullScreenDisable(true);
            JSONObject options = new JSONObject();
            options.put("name", "LiveFitLife");
            options.put("description", "Subscription Charges");
            options.put("order_id", razor_id);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", price);

            JSONObject preFill = new JSONObject();
            preFill.put("email", SharedpreferenceUtility.getInstance(this).getUser(SAVE_EMAIL));
            preFill.put("contact", SharedpreferenceUtility.getInstance(this).getUser(SAVE_PHONE));

            options.put("prefill", preFill);
            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {

        Log.e(TAG, "Exception in onPaymentSuccess " + s + "----Signature ----" + paymentData.getSignature());
     //   Toast.makeText(this, "Payment Successful: " + s + " Signature " + paymentData.getSignature(), Toast.LENGTH_SHORT).show();

        verifyOrder(paymentData.getPaymentId(), paymentData.getSignature(), paymentData.getOrderId(), razor_id);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Toast.makeText(this, "Payment failed: " + s + " " + paymentData.getOrderId(), Toast.LENGTH_SHORT).show();
    }

    public void getPlanDetails() {

        showProgressDialog();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("category_id", categoryId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_PLANBY_CATEGORY, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        Log.e("dataPlan", response.toString());
                        try {

                            arrayList = new ArrayList<>();
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONArray dataObject = data.getJSONArray(i);
                                SubscriptionModel m1 = new SubscriptionModel();

                                for (int j = 0; j < dataObject.length(); j++) {
                                    JSONObject jsonObject1 = dataObject.getJSONObject(j);

                                    String dietType = jsonObject1.getString("diet_type");
                                    m1.setDietType(dietType);
                                    
                                    categoryName = jsonObject1.getString("category_name");
                                    m1.setId(jsonObject1.getString("id"));
                                    m1.setHeaderTxt(jsonObject1.getString("category_name"));


                                    if (dietType.equals("WITHOUT")) {
                                        m1.setPricewithoutDiet("₹ "+jsonObject1.getString("price") + "\n(without DietPlan)");
                                        m1.setDayswithoutDiet(jsonObject1.getString("days") + " Days Pack");
                                        m1.setDescwithoutDiet(jsonObject1.getString("description"));
                                        m1.setPriceOnlywithoutDiet(jsonObject1.getString("price"));

                                    } else if (dietType.equals("WITH")) {
                                        m1.setPricewithDiet("₹ "+jsonObject1.getString("price") + "\n(with DietPlan)");
                                        m1.setDayswithDiet(jsonObject1.getString("days") + " Days Pack");
                                        m1.setDescwithDiet(jsonObject1.getString("description"));
                                        m1.setPriceOnlywithDiet(jsonObject1.getString("price"));

                                    } else if (dietType.equals("NA")) {
                                        if (categoryId.equals("4") || categoryId.equals("7")) {
                                            String clientType = jsonObject1.getString("client_type");
                                            m1.setClientType(clientType);
                                            if (clientType.equals("Single")) {
                                                m1.setPricewithoutDiet("₹ "+jsonObject1.getString("price") + "\n(DietPlan) " + clientType);
                                                m1.setDayswithoutDiet(jsonObject1.getString("days") + " Days Pack");
                                                m1.setDescwithoutDiet(jsonObject1.getString("description"));
                                                m1.setPriceOnlywithoutDiet(jsonObject1.getString("price"));

                                            } else if (clientType.equals("Couple")) {
                                                m1.setPricewithDiet("₹ "+jsonObject1.getString("price") + "\n(DietPlan) " + clientType);
                                                m1.setDayswithDiet(jsonObject1.getString("days") + " Days Pack");
                                                m1.setDescwithDiet(jsonObject1.getString("description"));
                                                m1.setPriceOnlywithDiet(jsonObject1.getString("price"));

                                            }

                                        }
                                        String clientType = jsonObject1.getString("client_type");
                                        m1.setClientType(clientType);
                                        if (clientType.equals("NA")) {
                                            m1.setPricewithoutDiet("₹ "+jsonObject1.getString("price") + "\n(DietPlan) ");
                                            m1.setDayswithoutDiet(jsonObject1.getString("days") + " Days Pack");
                                            m1.setDescwithoutDiet(jsonObject1.getString("description"));
                                            m1.setPricewithDiet("₹ "+jsonObject1.getString("price") + "\n(DietPlan) ");
                                            m1.setDayswithDiet(jsonObject1.getString("days") + " Days Pack");
                                            m1.setDescwithDiet(jsonObject1.getString("description"));
                                            m1.setPriceOnlywithoutDiet(jsonObject1.getString("price"));
                                            m1.setPriceOnlywithDiet(jsonObject1.getString("price"));

                                        }
                                    }


                                }
                                arrayList.add(m1);
                            }

                            SubscriptionAdapter subscriptionAdapter = new SubscriptionAdapter(SubscriptionActivity.this, arrayList, new OnClickBuyPlan() {

                                @Override
                                public void OnClickWithDiet(int position) {
                                    diet_typecount = "1";
                                    showProgressDialog();
                                    generateOrder(arrayList.get(position).getId(), arrayList.get(position).getPriceOnlywithDiet(), diet_typecount);
                                }

                                @Override
                                public void OnClickWithoutDiet(int position) {
                                    diet_typecount = "0";
                                    generateOrder(arrayList.get(position).getId(), arrayList.get(position).getPriceOnlywithoutDiet(), diet_typecount);

                                }
                            });

                            listOfSubscrption.setAdapter(subscriptionAdapter);

                        } catch (Exception e) {
                            Log.e("dataPlanException", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(SubscriptionActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(SubscriptionActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };
        Mysingleton.getInstance(SubscriptionActivity.this).addTorequestque(jsonObjReq);
    }


    public void generateOrder(String id, String price, String diettype_count) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("plan_id", id);
            jsonObject.put("with_diet", diettype_count);


            Log.e("orderId", jsonObject.toString());
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GENERATE_ORDER, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                         hideDialog();

                        Log.e("orderId", response.toString());

                        try {
                            JSONObject dataObject = response.getJSONObject("data");

                            order_id = dataObject.getString("order_id");
                            razor_id = dataObject.getString("razor_id");

                            Log.e(" ", razor_id);

                            startPayment(razor_id, Integer.parseInt(price));

                        } catch (Exception e) {

                            Log.e("orderId", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");
                            Log.e("orderId", message);
                            Toast.makeText(SubscriptionActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(SubscriptionActivity.this).getUser(SAVE_TOKEN));
                Log.e("orderId", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(SubscriptionActivity.this).addTorequestque(jsonObjReq);
    }

    public void verifyOrder(String paymentId, String signature, String RazorOrderID, String orderId) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("razorpay_payment_id", paymentId);
            jsonObject.put("razorpay_order_id", RazorOrderID);
            jsonObject.put("razorpay_signature", signature);
            jsonObject.put("order_id", order_id);

            Log.e("orderId", jsonObject.toString());
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.VERIFY_PAYMENT, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("orderId", response.toString());
                        //{"status":1,"message":"Payment verified successfully","data":{}}
                        try {

                            if (response.getString("status").equals("1")) {

                                Toast.makeText(SubscriptionActivity.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(SubscriptionActivity.this, BookSlotActivity.class);
                                intent.putExtra("categoryId", categoryId);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            Log.e("orderId", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");
                            Log.e("orderId", message);
                            Toast.makeText(SubscriptionActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(SubscriptionActivity.this).getUser(SAVE_TOKEN));
                Log.e("jsonObject", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(SubscriptionActivity.this).addTorequestque(jsonObjReq);
    }

}
