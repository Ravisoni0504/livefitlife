package com.android.livefitlife.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.CHECKPERMISSION;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_IMAGE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class EditProfileActivity extends BaseActivity {
    private static final int REQUEST_PERMISSIONS = 100;
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int CAPTURE_IMAGE = 4;
    String filePath = "", frontFilePath = "", sideFilePath = "", allowPermission,
            backFilePath = "", Name, Age, Height, Weight, MedicalHistory, Email, DietType, Gender, Count = "0";
    TextView changeImage;
    CircleImageView userImage;
    ImageView frontImage, sideImage, backImage;
    ImageView imageFemale, imageMale, imageVeg, imageNon;
    TextView textFemale, textMale, textVeg, textNon;
    Button submitButton;
    ArrayList<String> listOfImages = new ArrayList<>();
    CardView male, female, nonVegCard, vegCard, frontCard, sideCard, backCard;
    EditText name, age, height, weight, medicalHistory, mobile, email, address;

    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_porfile);

        userImage = findViewById(R.id.userImage);
        changeImage = findViewById(R.id.changeImage);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        weight = findViewById(R.id.weight);
        height = findViewById(R.id.height);
        medicalHistory = findViewById(R.id.medicalHistory);
        mobile = findViewById(R.id.mobile);
        email = findViewById(R.id.email);
        male = findViewById(R.id.maleCard);
        female = findViewById(R.id.femaleCard);
        vegCard = findViewById(R.id.vegCard);
        nonVegCard = findViewById(R.id.nonVegCard);
        imageFemale = findViewById(R.id.imagefemale);
        imageMale = findViewById(R.id.imagemale);
        textFemale = findViewById(R.id.textfemale);
        textMale = findViewById(R.id.textmale);
        imageNon = findViewById(R.id.imageNonVeg);
        imageVeg = findViewById(R.id.imageVeg);
        textNon = findViewById(R.id.textNonVeg);
        textVeg = findViewById(R.id.textVeg);
        frontCard = findViewById(R.id.frontCard);
        sideCard = findViewById(R.id.sideCard);
        backCard = findViewById(R.id.backCard);
        frontImage = findViewById(R.id.frontImage);
        backImage = findViewById(R.id.backImage);
        sideImage = findViewById(R.id.sideImage);
        submitButton = findViewById(R.id.submitButton);

        male.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Gender = "Male";
                male.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                textMale.setTextColor(Color.parseColor("#FFFFFF"));
                imageMale.setColorFilter(Color.parseColor("#FFFFFF"));
                female.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                textFemale.setTextColor(Color.parseColor("#BC0E83"));
                imageFemale.setColorFilter(Color.parseColor("#BC0E83"));
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gender = "Female";
                female.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                textFemale.setTextColor(Color.parseColor("#FFFFFF"));
                imageFemale.setColorFilter(Color.parseColor("#FFFFFF"));
                male.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                textMale.setTextColor(Color.parseColor("#BC0E83"));
                imageMale.setColorFilter(Color.parseColor("#BC0E83"));
            }
        });

        vegCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DietType = "Vegeterian";
                vegCard.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                textVeg.setTextColor(Color.parseColor("#FFFFFF"));
                imageVeg.setColorFilter(Color.parseColor("#FFFFFF"));
                nonVegCard.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                textNon.setTextColor(Color.parseColor("#BC0E83"));
                imageNon.setColorFilter(Color.parseColor("#BC0E83"));
            }
        });

        nonVegCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DietType = "NonVegeterian";
                nonVegCard.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                textNon.setTextColor(Color.parseColor("#FFFFFF"));
                imageNon.setColorFilter(Color.parseColor("#FFFFFF"));
                vegCard.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                textVeg.setTextColor(Color.parseColor("#BC0E83"));
                imageVeg.setColorFilter(Color.parseColor("#BC0E83"));

            }
        });

        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allowPermission.equals("success")) {
                    Count = "1";
                    selectImage();
                } else {
                    checkPermissions();
                }
            }
        });

        frontCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allowPermission.equals("success")) {
                    Count = "2";
                    selectImage();
                } else {
                    checkPermissions();
                }
            }
        });
        backCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allowPermission.equals("success")) {
                    Count = "3";
                    selectImage();
                } else {
                    checkPermissions();
                }
            }
        });
        sideCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allowPermission.equals("success")) {
                    Count = "4";
                    selectImage();
                } else {
                    checkPermissions();
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //    showProgressDialog();
                new EditProfile().execute();
            }
        });

        checkPermissions();
        getProfile();

    }

    private boolean checkPermissions() {
        int result, result1, result2, result3, result4;
        List<String> listPermissionsNeeded = new ArrayList<>();
        /*for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }*/
        result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        result3 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        result4 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED /*&& result3 == PackageManager.PERMISSION_GRANTED && result4 == PackageManager.PERMISSION_GRANTED*/) {
            allowPermission = "success";
        } else {
            allowPermission = "";
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (result1 != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (result2 != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
          /*  if (requestCode == 100) {
                int check = 0;
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            check = check + 1;
                        }
                    }
                    if (check == grantResults.length) {
                        SharedpreferenceUtility.getInstance(getApplicationContext()).putString(Constants.CHECKPERMISSION, "success");
                    } else {
                        SharedpreferenceUtility.getInstance(getApplicationContext()).putString(Constants.CHECKPERMISSION, "");
                    }
                }
                return;
            }
            return;*/
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                SharedpreferenceUtility.getInstance(getApplicationContext()).setUser(CHECKPERMISSION, "success");
                //showPictureDialog();
            } else {
                SharedpreferenceUtility.getInstance(getApplicationContext()).setUser(CHECKPERMISSION, "");
            }

            return;
        }
    }

    private void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        showCameraForImage();

                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();

                        showFileChooser();

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void onSubmitButton() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.CAMERA))) {

            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }

        } else {
            selectImage();
        }
    }

    private void showCameraForImage() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAPTURE_IMAGE);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            if (Count.equals("1")) {
                Uri picUri1 = data.getData();
                filePath = getPath(picUri1);
                // listOfImages.add(filePath);
                // Toast.makeText(this, "" + picUri1 + "\n\n" + frontFilePath, Toast.LENGTH_SHORT).show();
                //  Picasso.get().load("file://" + filePath).into(userImage);
                Picasso.get().load(picUri1).into(userImage);
            } else if (Count.equals("2")) {
                Uri picUri2 = data.getData();
                frontFilePath = getPath(picUri2);
                //  listOfImages.add(frontFilePath);
                //   Toast.makeText(this, "" + picUri2 + "\n\n" + frontFilePath, Toast.LENGTH_SHORT).show();
                //   Picasso.get().load("file://" + frontFilePath).into(frontImage);
                Picasso.get().load(picUri2).into(frontImage);
            } else if (Count.equals("3")) {
                Uri picUri3 = data.getData();
                backFilePath = getPath(picUri3);
                //  listOfImages.add(backFilePath);

                //   Toast.makeText(this, "" + picUri3 + "\n\n" + frontFilePath, Toast.LENGTH_SHORT).show();
                //Picasso.get().load("file://" + backFilePath).into(backImage);
                Picasso.get().load(picUri3).into(backImage);
            } else if (Count.equals("4")) {
                Uri picUri4 = data.getData();
                sideFilePath = getPath(picUri4);
                //  listOfImages.add(sideFilePath);
                //   Toast.makeText(this, "" + picUri4 + "\n\n" + frontFilePath, Toast.LENGTH_SHORT).show();
                //  Picasso.get().load("file://" + sideFilePath).into(sideImage);
                Picasso.get().load(picUri4).into(sideImage);
            }

            // listOfImages.add(filePath);
            //  serviceImageAdapter.notifyItemInserted(listOfImages.size() - 1);

        } else if (requestCode == CAPTURE_IMAGE && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            if (Count.equals("1")) {
                Uri tempUri1 = getImageUri(getApplicationContext(), photo);
                filePath = getRealPathFromURI(tempUri1);
                //    listOfImages.add(filePath);
                // Picasso.get().load("file://" + filePath).into(userImage);
                Picasso.get().load(tempUri1).into(userImage);
            } else if (Count.equals("2")) {
                Uri tempUri2 = getImageUri(getApplicationContext(), photo);
                frontFilePath = getRealPathFromURI(tempUri2);
                //   listOfImages.add(frontFilePath);
                //   Picasso.get().load("file://" + frontFilePath).into(frontImage);
                Picasso.get().load(tempUri2).into(frontImage);
            } else if (Count.equals("3")) {
                Uri tempUri3 = getImageUri(getApplicationContext(), photo);
                backFilePath = getRealPathFromURI(tempUri3);
                //   listOfImages.add(backFilePath);
                //    Picasso.get().load("file://" + backFilePath).into(backImage);
                Picasso.get().load(tempUri3).into(backImage);
            } else if (Count.equals("4")) {
                Uri tempUri4 = getImageUri(getApplicationContext(), photo);
                sideFilePath = getRealPathFromURI(tempUri4);
                //   listOfImages.add(sideFilePath);
//                Picasso.get().load("file://" + sideFilePath).into(sideImage);
                Picasso.get().load(tempUri4).into(sideImage);
            }

            //listOfImages.add(filePath);
            //serviceImageAdapter.notifyItemInserted(listOfImages.size() - 1);
        }

    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title" + System.currentTimeMillis(), null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public void getProfile() {

        showProgressDialog();
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_PROFILE, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("Response", response.toString());
                        hideDialog();
                        try {
                            JSONObject dataObject = response.getJSONObject("data");

                            email.setText(dataObject.getString("email"));
                            name.setText(dataObject.getString("name"));
                            age.setText(dataObject.getString("age"));
                            height.setText(dataObject.getString("height"));
                            weight.setText(dataObject.getString("weight"));
                            mobile.setText(dataObject.getString("phone"));
                            medicalHistory.setText(dataObject.getString("medical_history"));

                            String imagefilePath = dataObject.getString("image");
                            String imagefrontfilePath = dataObject.getString("image_front");
                            String imagesidefilePath = dataObject.getString("image_side");
                            String imagebackfilePath = dataObject.getString("image_back");

                            SharedpreferenceUtility.getInstance(EditProfileActivity.this).setUser(SAVE_IMAGE, imagefilePath);

                            if (imagefilePath != null) {
                                userImage.setBorderColor(getResources().getColor(R.color.orangetheme));
                                Picasso.get().load(imagefilePath).placeholder(R.drawable.defaultt).into(userImage);
                            }
                            if (imagefrontfilePath != null) {
                                Picasso.get().load(imagefrontfilePath).placeholder(R.drawable.defaultt).into(frontImage);
                            }

                            if (imagesidefilePath != null) {
                                Picasso.get().load(imagesidefilePath).placeholder(R.drawable.defaultt).into(sideImage);
                            }

                            if (imagebackfilePath != null) {
                                Picasso.get().load(imagebackfilePath).placeholder(R.drawable.defaultt).into(backImage);
                            }
                            Gender = dataObject.getString("gender");
                            if (Gender.equals("Male")) {
                                male.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                                textMale.setTextColor(Color.parseColor("#FFFFFF"));
                                imageMale.setColorFilter(Color.parseColor("#FFFFFF"));
                                female.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                                textFemale.setTextColor(Color.parseColor("#BC0E83"));
                                imageFemale.setColorFilter(Color.parseColor("#BC0E83"));
                            }
                            if (Gender.equals("Female")) {
                                female.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                                textFemale.setTextColor(Color.parseColor("#FFFFFF"));
                                imageFemale.setColorFilter(Color.parseColor("#FFFFFF"));
                                male.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                                textMale.setTextColor(Color.parseColor("#BC0E83"));
                                imageMale.setColorFilter(Color.parseColor("#BC0E83"));
                            }

                            DietType = dataObject.getString("diet_type");
                            if (DietType.equals("Vegeterian")) {
                                vegCard.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                                textVeg.setTextColor(Color.parseColor("#FFFFFF"));
                                imageVeg.setColorFilter(Color.parseColor("#FFFFFF"));
                                nonVegCard.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                                textNon.setTextColor(Color.parseColor("#BC0E83"));
                                imageNon.setColorFilter(Color.parseColor("#BC0E83"));
                            }
                            if (DietType.equals("NonVegeterian")) {
                                nonVegCard.setCardBackgroundColor(Color.parseColor("#BC0E83"));
                                textNon.setTextColor(Color.parseColor("#FFFFFF"));
                                imageNon.setColorFilter(Color.parseColor("#FFFFFF"));
                                vegCard.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                                textVeg.setTextColor(Color.parseColor("#BC0E83"));
                                imageVeg.setColorFilter(Color.parseColor("#BC0E83"));
                            }


                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            //      Toast.makeText(EditProfileActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(EditProfileActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(EditProfileActivity.this).addTorequestque(jsonObjReq);
    }

    private String doFileUpload() {

        Email = email.getText().toString();
        Name = name.getText().toString();
        Age = age.getText().toString();
        Height = height.getText().toString();
        Weight = weight.getText().toString();
        MedicalHistory = medicalHistory.getText().toString();

        String response_str = "";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(URLs.EDIT_PROFILE);

            MultipartEntity reqEntity = new MultipartEntity();

            /*for (int i = 0; i < listOfImages.size(); i++) {
                File file3 = new File(listOfImages.get(i));
                FileBody fileBody3 = new FileBody(file3);
                reqEntity.addPart("file[]", fileBody3);
            }
*/
            if (filePath != "") {
                File file = new File(filePath);
                FileBody fileBody = new FileBody(file);
                reqEntity.addPart("file", fileBody);
            }

            if (frontFilePath != "") {
                File file2 = new File(frontFilePath);
                FileBody fileBody2 = new FileBody(file2);
                reqEntity.addPart("front", fileBody2);
            }

            if (sideFilePath != "") {
                File file3 = new File(sideFilePath);
                FileBody fileBody3 = new FileBody(file3);
                reqEntity.addPart("side", fileBody3);
            }

            if (backFilePath != "") {
                File file4 = new File(backFilePath);
                FileBody fileBody4 = new FileBody(file4);
                reqEntity.addPart("back", fileBody4);
            }

            //  }

            reqEntity.addPart("name", new StringBody(Name));
            reqEntity.addPart("age", new StringBody(Age));
            reqEntity.addPart("height", new StringBody(Height));
            reqEntity.addPart("weight", new StringBody(Weight));
            reqEntity.addPart("medicalHistory", new StringBody(MedicalHistory));
            reqEntity.addPart("mobile", new StringBody(mobile.getText().toString()));
            reqEntity.addPart("email", new StringBody(Email));
            reqEntity.addPart("gender", new StringBody(Gender));
            reqEntity.addPart("diet_type", new StringBody(DietType));

            post.addHeader("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(EditProfileActivity.this).getUser(SAVE_TOKEN));

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();

            response_str = EntityUtils.toString(resEntity);
            Log.d("Debug", String.valueOf(reqEntity));


        } catch (Exception ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
        return response_str;
    }

    public class EditProfile extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String response_str = doFileUpload();
            return response_str;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {

            Log.d("response", String.valueOf(s));
            hideDialog();
            try {
                JSONObject jsonObject = new JSONObject(s);


                JSONObject data = jsonObject.getJSONObject("data");
                SharedpreferenceUtility.getInstance(EditProfileActivity.this).setUser(SAVE_IMAGE, data.getString("image"));


                Intent intent = new Intent(getApplicationContext(), ProfileAcitivty.class);
                startActivity(intent);
                Toast.makeText(EditProfileActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


            } catch (Exception e) {

                Toast.makeText(EditProfileActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(s);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EditProfileActivity.this.finish();
    }
}
