package com.android.livefitlife.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class RegisterActivity extends BaseActivity {
    Button buttonRegister;
    LinearLayout persnlBlock, bodyBlock;
    ImageView backBtn;
    RadioGroup radiogroupGender;
    EditText name, email, age, phone;

    String socialNameStr = "", socialId = "", social_type = "", selectedGender = "", device_token = "";
    TextView socialWelcome, socialName;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Intent intent = getIntent();

        socialWelcome = findViewById(R.id.socialWelcome);
        socialName = findViewById(R.id.socialName);

        socialWelcome.setVisibility(View.GONE);
        socialName.setVisibility(View.GONE);

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);

        age = findViewById(R.id.age);
        phone = findViewById(R.id.phone);

        device_token = intent.getStringExtra("device_token");
        Log.d("token", device_token);

        if (intent.hasExtra("phoneNumber")) {
            String phoneNumber = intent.getStringExtra("phoneNumber");
            phone.setText(phoneNumber);
        }

        if (intent.hasExtra("userId")) {

            social_type = intent.getStringExtra("social_type");
            socialId = intent.getStringExtra("userId"); //, social_userID);
            socialNameStr = intent.getStringExtra("userName"); //, social_userName);
            name.setVisibility(View.GONE);

            socialWelcome.setVisibility(View.VISIBLE);
            socialName.setVisibility(View.VISIBLE);
            socialName.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_view));
            socialWelcome.setText("Welcome , " + socialNameStr + " ! ");
        }

        buttonRegister = findViewById(R.id.buttonRegister);
        radiogroupGender = findViewById(R.id.radiogroupGender);

        radiogroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioGroup = findViewById(checkedId);
                selectedGender = radioGroup.getText().toString();
            }
        });

        intiliaseView();

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!socialId.equals("")) {
                    if (selectedGender.equals("") || isEmpty(phone) || isEmpty(email) || isEmpty(age)) {

                        Toast.makeText(RegisterActivity.this, "Please Enter All Details", Toast.LENGTH_SHORT).show();

                    } else {
                        showProgressDialog();
                        registerUser();
                    }
                } else {

                    if (selectedGender.equals("") || isEmpty(name) || isEmpty(phone) || isEmpty(email) || isEmpty(age)) {
                        Toast.makeText(RegisterActivity.this, "Please Enter All Details", Toast.LENGTH_SHORT).show();
                    } else {
                        showProgressDialog();
                        registerUser();
                    }
                }
            }
        });
    }

    public boolean isEmpty(EditText e) {
        boolean check = false;
        if (e.getText().toString().trim().length() == 0) {
            check = true;
        }
        return check;
    }

    public void intiliaseView() {
        backBtn = findViewById(R.id.backBtn);
        persnlBlock = findViewById(R.id.persnlBlock);
        bodyBlock = findViewById(R.id.bodyBlock);
    }


    public void registerUser() {

        JSONObject jsonObject = new JSONObject();
        try {
            RadioButton radioButton = findViewById(radiogroupGender.getCheckedRadioButtonId());
            if (!socialId.equals("")) {
                jsonObject.put("social_type", social_type);
                jsonObject.put("social_id", socialId);
                jsonObject.put("name", socialNameStr);

            } else {
                jsonObject.put("name", name.getText().toString());
            }

            jsonObject.put("email", email.getText().toString());
            jsonObject.put("age", age.getText().toString());
            jsonObject.put("phone", phone.getText().toString());
            jsonObject.put("gender", "" + radioButton.getText());
            jsonObject.put("device_id", device_token);

            Log.e("dataRegister", jsonObject.toString());
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.REGISTER, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();

                        Log.e("dataRegister", response.toString());

                        try {

                            String status = response.getString("status");
                            String message = response.getString("message");

                            if (status.equals("1")) {
                                Toast.makeText(RegisterActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(RegisterActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {

                        }

                        //"data": {
                        // 		"user": {
                        // 			"name": "Dazz Bhaiya",
                        // 			"email": "dazzlerbaba0001@gmail.com",
                        // 			"phone": "8003222231",
                        // 			"age": "25",
                        // 			"gender": "Male",
                        //}
                        // 		"token": ".cbBRpGWW7yvde55e4e834TZaMd3DWOGLqxFfqDupTAA"


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(RegisterActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {
        };

        Mysingleton.getInstance(RegisterActivity.this).addTorequestque(jsonObjReq);
    }
}
