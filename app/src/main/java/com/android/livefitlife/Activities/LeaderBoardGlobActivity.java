package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.R;
import com.android.livefitlife.Adapters.UserListAdapter;
import com.android.livefitlife.Models.UserModel;

import java.util.ArrayList;

public class LeaderBoardGlobActivity extends AppCompatActivity {
    RecyclerView listUser;
    ArrayList<UserModel> arrayList = new ArrayList<>();
    CardView btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board_glob);
        btnHome = findViewById(R.id.btnHome);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LeaderBoardGlobActivity.this, NavDashActivity.class);
                startActivity(intent);
            }
        });

        listUser = findViewById(R.id.listUser);
        listUser.setLayoutManager(new LinearLayoutManager(this));

        UserListAdapter userListAdapter = new UserListAdapter(this, arrayList);
        listUser.setAdapter(userListAdapter);

    }
}
