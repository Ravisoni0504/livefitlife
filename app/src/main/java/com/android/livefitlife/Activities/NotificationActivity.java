package com.android.livefitlife.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.DietChartAdapter;
import com.android.livefitlife.Adapters.NotificationAdapter;
import com.android.livefitlife.Models.DietChartModel;
import com.android.livefitlife.Models.NotificationModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickDietPlan;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class NotificationActivity extends BaseActivity {

    RecyclerView listview;
    ArrayList<NotificationModel> arrayList;
    ImageView back_button;
    TextView noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        listview = findViewById(R.id.notificationActivity);
        listview.setLayoutManager(new LinearLayoutManager(this));

        back_button = findViewById(R.id.back_button);
        noDataText=findViewById(R.id.noDataText);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        getNotificationList();

    }


    public void getNotificationList() {

        showProgressDialog();
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_NOTIFICATION, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");
                            if (data.length() != 0) {
                                Log.e("response", data.toString());

                                arrayList = new ArrayList<>();

                                for (int i = 0; i < data.length(); i++) {

                                    JSONObject dataObject = data.getJSONObject(i);

                                    NotificationModel notificationModel = new NotificationModel();
                                    notificationModel.setTitle(dataObject.getString("title"));
                                    notificationModel.setDescription(dataObject.getString("message"));
                                 /*   String date= dataObject.getString("created_at");

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                    SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
                                    Date d = sdf.parse(date);
                                    String formattedDate = output.format(d);
*/
                                    notificationModel.setTimeStamp(dataObject.getString("created_at"));

                                    arrayList.add(notificationModel);

                                }

                                noDataText.setVisibility(View.GONE);
                                listview.setVisibility(View.VISIBLE);
                                NotificationAdapter notificationAdapter = new NotificationAdapter(NotificationActivity.this, arrayList);
                                listview.setAdapter(notificationAdapter);
                            } else {
                                noDataText.setVisibility(View.VISIBLE);
                                listview.setVisibility(View.GONE);
                            }

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(NotificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(NotificationActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(NotificationActivity.this).addTorequestque(jsonObjReq);
    }
}