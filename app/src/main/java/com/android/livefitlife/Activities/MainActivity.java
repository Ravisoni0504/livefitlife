package com.android.livefitlife.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.android.livefitlife.R;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.bitvale.lightprogress.LightProgress;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class MainActivity extends AppCompatActivity {


    String isLogin = "";
    RelativeLayout rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rootView = findViewById(R.id.rootView);

        final LightProgress light = findViewById(R.id.light);

        if (!light.isOn()) {
            light.on();
        } else {
            light.off();
        }


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                isLogin = SharedpreferenceUtility.getInstance(MainActivity.this).getUser(SAVE_TOKEN);

                if (isLogin.equals("") || isLogin.equals("0")) {
                    Intent i = new Intent(MainActivity.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent intent = new Intent(MainActivity.this, NavDashActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

        }, 5000);

        printHashKey(MainActivity.this);

    }

    public static void printHashKey(Context pContext) {
        try {

            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {

                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));

                Log.i("TAGHash", "printHashKey() Hash Key: " + hashKey);
            }

        } catch (NoSuchAlgorithmException e) {
            Log.e("TAGHash", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("TAGHash", "printHashKey()", e);
        }
    }

}
