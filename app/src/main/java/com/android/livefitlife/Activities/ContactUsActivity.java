package com.android.livefitlife.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.livefitlife.Adapters.NotificationAdapter;
import com.android.livefitlife.Models.NotificationModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class ContactUsActivity extends BaseActivity {

    // String urlContact = "http://livefitlife.in/contact/";

    EditText name, email, number, message;
    ImageView back_button;
    Button helpMee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        back_button = findViewById(R.id.back_button);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        number = findViewById(R.id.contactNumber);
        message = findViewById(R.id.message);

        helpMee = findViewById(R.id.helpme);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        helpMee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEmpty(name) || isEmpty(number) || isEmpty(email) || isEmpty(message)) {

                    Toast.makeText(ContactUsActivity.this, "Please Enter All Details", Toast.LENGTH_SHORT).show();

                } else {
                    showProgressDialog();
                    postContactUs();
                }

            }
        });


     /*   WebView webView = findViewById(R.id.webView);

    // specify the url of the web page in loadUrl function
        webView.setWebViewClient(new MyWebViewClient());
    // string url which you have to load into a web view
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(urlContact);*/
    }

   /* // custom web view client class who extends WebViewClient
    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgressDialog();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideDialog();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.equals(urlContact))
                view.loadUrl(url); // load the url
            return true;
        }
    }*/

    public void postContactUs() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name.getText().toString());
            jsonObject.put("email", email.getText().toString());
            jsonObject.put("phone", number.getText().toString());
            jsonObject.put("message", message.getText().toString());
        } catch (Exception e) {

        }
        Log.d(TAG, jsonObject.toString() + "--- Add responce  ----" + jsonObject);

        showProgressDialog();
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.CONTACT_US, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(ContactUsActivity.this, "Your query has been sent", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(getApplicationContext(), NavDashActivity.class);
                            startActivity(intent);
                            ContactUsActivity.this.finish();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(ContactUsActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(ContactUsActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(ContactUsActivity.this).addTorequestque(jsonObjReq);
    }

    public boolean isEmpty(EditText e) {
        boolean check = false;
        if (e.getText().toString().trim().length() == 0) {
            check = true;
        }
        return check;
    }
}