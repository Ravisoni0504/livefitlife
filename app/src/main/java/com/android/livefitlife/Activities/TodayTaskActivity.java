package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.FullScheduleAdapter;
import com.android.livefitlife.Adapters.TodayScheduleAdapter;
import com.android.livefitlife.Models.FullScheduleModel;
import com.android.livefitlife.Models.TodayTaskModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.livefitlife.VideoCalling.VideoActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class TodayTaskActivity extends BaseActivity {

    String room_name, token, BY;
    RelativeLayout joinClass;
    ImageView back_button;
    RecyclerView today_schedule_recycler;
    ArrayList<TodayTaskModel> todayTaskModels;
    TextView fullSchedule,schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_task);

        schedule=findViewById(R.id.schedule);
        fullSchedule = findViewById(R.id.fullSchedule);
        schedule.setVisibility(View.GONE);

        today_schedule_recycler = findViewById(R.id.today_schedule_recycler);
        today_schedule_recycler.setLayoutManager(new LinearLayoutManager(this));

        Intent intent = getIntent();
        if (intent.hasExtra("BY")) {
            BY = intent.getStringExtra("BY");
        }

        back_button = findViewById(R.id.back_button);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (BY.equals("Dashboard")) {
            getTDYSchedule();
        } else {
            getTDYScheduleByCategory();
        }


        fullSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogFullSchedule();
            }
        });
    }

    public void showDialogFullSchedule() {

        ArrayList<FullScheduleModel> arrayList = new ArrayList<>();
        View view = getLayoutInflater().inflate(R.layout.fullscheduledialog, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        RecyclerView fullscheduleList = dialog.findViewById(R.id.fullscheduleList);
        fullscheduleList.setLayoutManager(new LinearLayoutManager(this));


        FullScheduleAdapter fullScheduleAdapter = new FullScheduleAdapter(this, arrayList);
        fullscheduleList.setAdapter(fullScheduleAdapter);

    }

    public void getTDYSchedule() {
        showProgressDialog();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_SCHEDULE_TDY, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ResponseTrainer", response.toString());
                        hideDialog();
                        todayTaskModels = new ArrayList<>();
                        try {
                            String status = response.getString("status");

                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                TodayTaskModel todayTaskModel = new TodayTaskModel();

                                JSONObject jsonObject = data.getJSONObject(i);

                                String trainer_name = jsonObject.getString("trainer_name");
                                String trainer_photo = jsonObject.getString("trainer_image");
                                String trainer_id = jsonObject.getString("trainer_id");
                                String time = jsonObject.getString("start");
                                String category_name = jsonObject.getString("category_name");
                                String room_name = jsonObject.getString("room_name");
                               /* if (category_name.contains(",")) {
                                    category_name.replace(",", " | ");
                                }*/

                                Date date = null;
                                try {
                                    date = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss").parse(time);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String convertTime = new SimpleDateFormat("HH:mm").format(date);

                                todayTaskModel.setTrainer_id(trainer_id);
                                todayTaskModel.setTrainerName(trainer_name);
                                todayTaskModel.setTrainerPhoto(trainer_photo);
                                todayTaskModel.setTime(convertTime);
                                todayTaskModel.setTrainerCategory(category_name);
                                todayTaskModel.setRoomName(room_name);

                                todayTaskModels.add(todayTaskModel);

                            }
                            TodayScheduleAdapter todayScheduleAdapter = new TodayScheduleAdapter(getApplicationContext(), todayTaskModels, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {
                                    getAccessToken(todayTaskModels.get(position).getRoomName());

                                }
                            });
                            today_schedule_recycler.setAdapter(todayScheduleAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("ResponseTrainer", error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Log.d("ResponseTrainer", message);

                            Toast.makeText(TodayTaskActivity.this, "" + message, Toast.LENGTH_SHORT).show();

                            error.printStackTrace();

                        } catch (Exception e) {
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(TodayTaskActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;
            }
        };

        Mysingleton.getInstance(TodayTaskActivity.this).addTorequestque(jsonObjReq);
    }

    public void getTDYScheduleByCategory() {
        showProgressDialog();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category_id", BY);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_SCHEDULE_TDY, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ResponseTrainer", response.toString());
                        hideDialog();
                        todayTaskModels = new ArrayList<>();
                        try {
                            String status = response.getString("status");

                            JSONArray data = response.getJSONArray("data");
                            if (data.length()!=0){

                            for (int i = 0; i < data.length(); i++) {
                                TodayTaskModel todayTaskModel = new TodayTaskModel();

                                JSONObject jsonObject = data.getJSONObject(i);

                                String trainer_name = jsonObject.getString("trainer_name");
                                String trainer_photo = jsonObject.getString("trainer_image");
                                String trainer_id = jsonObject.getString("trainer_id");
                                String time = jsonObject.getString("start");
                                String category_name = jsonObject.getString("category_name");
                                String room_name = jsonObject.getString("room_name");
                               /* if (category_name.contains(",")) {
                                    category_name.replace(",", " | ");
                                }*/

                                Date date = null;
                                try {
                                    date = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss").parse(time);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String convertTime = new SimpleDateFormat("HH:mm").format(date);

                                todayTaskModel.setTrainer_id(trainer_id);
                                todayTaskModel.setTrainerName(trainer_name);
                                todayTaskModel.setTrainerPhoto(trainer_photo);
                                todayTaskModel.setTime(convertTime);
                                todayTaskModel.setTrainerCategory(category_name);
                                todayTaskModel.setRoomName(room_name);

                                todayTaskModels.add(todayTaskModel);

                            }
                            TodayScheduleAdapter todayScheduleAdapter = new TodayScheduleAdapter(getApplicationContext(), todayTaskModels, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {
                                    getAccessToken(todayTaskModels.get(position).getRoomName());

                                }
                            });
                            today_schedule_recycler.setAdapter(todayScheduleAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("ResponseTrainer", error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Log.d("ResponseTrainer", message);

                            Toast.makeText(TodayTaskActivity.this, "" + message, Toast.LENGTH_SHORT).show();

                            error.printStackTrace();

                        } catch (Exception e) {
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(TodayTaskActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;
            }
        };

        Mysingleton.getInstance(TodayTaskActivity.this).addTorequestque(jsonObjReq);
    }

    private void getAccessToken(String room_name) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("room_name", room_name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_ACCESS_TOKEN, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            token = response.getString("data");
                            Intent intent = new Intent(TodayTaskActivity.this, VideoActivity.class);
                            intent.putExtra("room", room_name);
                            intent.putExtra("tokn", token);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(TodayTaskActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(TodayTaskActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(TodayTaskActivity.this).addTorequestque(jsonObjReq);
    }


}
