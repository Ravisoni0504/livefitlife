package com.android.livefitlife.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.livefitlife.Adapters.MySubscriptionAdapter;
import com.android.livefitlife.Models.MySubscriptionModel;
import com.android.livefitlife.R;
import com.android.livefitlife.SubscriptionActivity;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class MySubscription extends BaseActivity {

    ImageView back_button;
    RecyclerView mysubscription_recycler;
    ArrayList<MySubscriptionModel> mySubscriptionModels;
    TextView noText;
    Button browseAllPlans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscription);

        back_button = findViewById(R.id.back_button);

        browseAllPlans = findViewById(R.id.browseAllPlans);
        noText = findViewById(R.id.noText);
        mysubscription_recycler = findViewById(R.id.mysubscription_recycler);
        mysubscription_recycler.setLayoutManager(new LinearLayoutManager(this));

        browseAllPlans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), WorkOutActivityList.class);
                startActivity(intent);
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSubscription();
    }


    public void getSubscription() {
        showProgressDialog();

        mySubscriptionModels = new ArrayList<>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_SUBSCRIPTION, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("responseSub", response.toString());

                        hideDialog();
                        try {

                            JSONArray data = response.getJSONArray("data");
                            if (data.length() != 0) {
                                for (int i = 0; i < data.length(); i++) {

                                    MySubscriptionModel mySubscriptionModel = new MySubscriptionModel();
                                    JSONObject jsonObject = data.getJSONObject(i);

                                    String categoryName = jsonObject.getString("category_name");
                                    String plan_id = jsonObject.getString("plan_id");
                                    String plan_name = jsonObject.getString("plan_name");
                                    String status = jsonObject.getString("status");
                                    String start_date = jsonObject.getString("start_date");
                                    String end_date = jsonObject.getString("next_bill_date");
                                    String price = jsonObject.getString("plan_price");
                                    String plan_days = jsonObject.getString("plan_days");

                                    if (status.equals("active")) {
                                        mySubscriptionModel.setPlan_category(categoryName);
                                        mySubscriptionModel.setPlan_id(plan_id);
                                        mySubscriptionModel.setPlan_name(plan_name);
                                        mySubscriptionModel.setStatus("Active");
                                        mySubscriptionModel.setStart_date(start_date);
                                        mySubscriptionModel.setEnd_date(end_date);
                                        mySubscriptionModel.setPrice(price);
                                        mySubscriptionModel.setPlan_days(plan_days);

                                        mySubscriptionModels.add(mySubscriptionModel);

                                    }


                                }

                                noText.setVisibility(View.GONE);
                                mysubscription_recycler.setVisibility(View.VISIBLE);
                                MySubscriptionAdapter mySubscriptionAdapter = new MySubscriptionAdapter(getApplicationContext(), mySubscriptionModels);
                                mysubscription_recycler.setAdapter(mySubscriptionAdapter);
                            } else {
                                noText.setVisibility(View.VISIBLE);
                                mysubscription_recycler.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            Log.e("responseSub", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");
                            Log.e("responseSub", message);
                            Toast.makeText(MySubscription.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(MySubscription.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(MySubscription.this).addTorequestque(jsonObjReq);
    }
}