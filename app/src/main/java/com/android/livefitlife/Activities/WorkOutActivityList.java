package com.android.livefitlife.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.ActivityAdapter;
import com.android.livefitlife.LevelListActivity;
import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.R;
import com.android.livefitlife.SubscriptionActivity;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.SpaceItem;
import com.android.livefitlife.Utility.URLs;
import com.android.livefitlife.roomdb.DatabaseClient;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class WorkOutActivityList extends BaseActivity {

    ArrayList<ActivityModel> arrayList;
    RecyclerView activityList;
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_out_list);

        backButton = findViewById(R.id.back_button);
        activityList = findViewById(R.id.activityList);
        activityList.setLayoutManager(new GridLayoutManager(this, 2));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getActivitiesList();
    }


    public void getActivitiesList() {
        showProgressDialog();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            arrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                ActivityModel activityModel = new ActivityModel();
                                activityModel.setActivityId(dataObject.getString("id"));
                                activityModel.setIs_lfl(dataObject.getString("is_lfl"));
                                activityModel.setDescription(dataObject.getString("description"));
                                activityModel.setActivityName(dataObject.getString("name"));
                                activityModel.setActivityIcon(dataObject.getString("image"));

                                arrayList.add(activityModel);

                            }

                            ActivityAdapter activityAdapter = new ActivityAdapter(WorkOutActivityList.this, arrayList, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {

                                    String isLFL = arrayList.get(position).getIs_lfl();

                                    getSpecificStatus(WorkOutActivityList.this, arrayList.get(position).getActivityId(), isLFL, position);
                                }
                            });
                            int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);

                            activityList.addItemDecoration(new SpaceItem(spacingInPixels));
                            activityList.setAdapter(activityAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(WorkOutActivityList.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(WorkOutActivityList.this).addTorequestque(jsonObjReq);

    }

    public void getSpecificStatus(final Context context, String id, String isLFL, int position) {

        class SaveTask extends AsyncTask<Void, Void, String> {

            @Override
            public String doInBackground(Void... voids) {
                //creating a task

                String idOfStatus = DatabaseClient.getInstance(context).getAppDatabase()
                        .taskDao()
                        .getStatusOfCategory(id);

                return idOfStatus;
            }

            @Override
            public void onPostExecute(String result) {
                super.onPostExecute(result);
                //      Toast.makeText(context, "status" + result, Toast.LENGTH_LONG).show();
                if (isLFL.equals("0")) {
                    if (result.equals("0")) {
                        Intent intent = new Intent(WorkOutActivityList.this, SubscriptionActivity.class);
                        intent.putExtra("categoryID", arrayList.get(position).getActivityId());
                        intent.putExtra("trainingType", "");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(WorkOutActivityList.this, TodayTaskActivity.class);
                        intent.putExtra("BY", arrayList.get(position).getActivityId());
                        startActivity(intent);
                    }
                } else {
                    if (result.equals("0")) {
                        Intent intent = new Intent(WorkOutActivityList.this, WorkoutActivity.class);
                        intent.putExtra("categoryID", arrayList.get(position).getActivityId());
                        intent.putExtra("trainingType", "");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(WorkOutActivityList.this, LevelListActivity.class);
                        intent.putExtra("categoryID", arrayList.get(position).getActivityId());
                        intent.putExtra("trainingType", "");
                        startActivity(intent);
                    }
                }
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }
}