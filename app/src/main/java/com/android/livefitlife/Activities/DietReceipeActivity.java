package com.android.livefitlife.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.VideoListDietAdapter;
import com.android.livefitlife.Models.DietRecipeModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class DietReceipeActivity extends BaseActivity {
    RecyclerView recipesList;
    ArrayList<DietRecipeModel> arrayList;
    ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_receipe);

        back_button = findViewById(R.id.back_button);
        recipesList = findViewById(R.id.recipesList);
        recipesList.setLayoutManager(new LinearLayoutManager(this));

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getReceipeList();
    }

    public void getReceipeList() {
        showProgressDialog();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.RECEIPE_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {

                            JSONArray data = response.getJSONArray("data");
                            Log.e("response", data.toString());
                            arrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                DietRecipeModel dietReceipe = new DietRecipeModel();
                                dietReceipe.setIdDietReceipe(dataObject.getString("id"));
                                dietReceipe.setYoutubeLink(dataObject.getString("youtube_link"));
                                dietReceipe.setNametitle(dataObject.getString("name"));

                                arrayList.add(dietReceipe);
                            }

                            VideoListDietAdapter videoListDietAdapter = new VideoListDietAdapter(DietReceipeActivity.this, arrayList);
                            recipesList.setAdapter(videoListDietAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(DietReceipeActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(DietReceipeActivity.this).addTorequestque(jsonObjReq);

    }
}
