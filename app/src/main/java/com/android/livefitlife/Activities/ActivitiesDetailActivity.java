package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.TrainerAdapter;
import com.android.livefitlife.Models.TrainerModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickDietPlan;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class ActivitiesDetailActivity extends BaseActivity {
    RecyclerView trainerList;
    ArrayList<TrainerModel> arrayList;
    String categoryId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities_detail);

        trainerList = findViewById(R.id.trainerList);
        trainerList.setLayoutManager(new LinearLayoutManager(this));

        Intent intent = getIntent();

        if (intent.hasExtra("id")) {
            categoryId = intent.getStringExtra("id");
            Toast.makeText(ActivitiesDetailActivity.this, "" + categoryId, Toast.LENGTH_SHORT).show();
            showProgressDialog();
            arrayList = new ArrayList<>();
            getTrainerList(categoryId);
        }

    }

    public void getTrainerList(String activityId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category_id", activityId);
        } catch (Exception e) {

        }
        Log.d(TAG, jsonObject.toString() + "--- Add responce  ----" + jsonObject);
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_TRAINER_BY_ID, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                TrainerModel activityModel = new TrainerModel();
                                activityModel.setTrainerId("user_id");
                                activityModel.setTrainerName(dataObject.getString("username"));
                                activityModel.setTrainerExpertise(dataObject.getString("category_name"));
                                activityModel.setTrainerRating(dataObject.getString("rating"));
                                activityModel.setTrainerImage(dataObject.getString("userimage"));
                                activityModel.setTrainerExperince("Experience : " + dataObject.getString("experience") + " Years");
                                arrayList.add(activityModel);

                            }

                            TrainerAdapter activityAdapter = new TrainerAdapter(ActivitiesDetailActivity.this, arrayList, new OnClickDietPlan() {
                                @Override
                                public void OnClickDiet(int position) {

                                }
                            });

                            trainerList.setAdapter(activityAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(ActivitiesDetailActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(ActivitiesDetailActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(ActivitiesDetailActivity.this).addTorequestque(jsonObjReq);
    }
}
