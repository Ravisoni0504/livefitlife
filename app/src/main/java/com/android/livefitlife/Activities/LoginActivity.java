package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_AGE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_EMAIL;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_GENDER;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_ID;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_NAME;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_PHONE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_USER_ID;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    TextView signUp;
    CardView hitButton;
    EditText phone, otp;
    String secondTime = "false", token;
    GoogleSignInAccount acct;
    private static final int RC_SIGN_IN = 007;
    private GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient;
    LoginButton loginButtonFB;

    RelativeLayout google, facebook;

    CallbackManager callbackManager;

    String social_userName = "", social_userID = "";

    String SocialEmailid, SocialFirstname, SocialLastname, SocialUserid, gender,
            birthday, SocialType, Userid, full_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        google = findViewById(R.id.google);
        hitButton = findViewById(R.id.hitButton);
        loginButtonFB = findViewById(R.id.loginButtonFB);
        phone = findViewById(R.id.phone);
        otp = findViewById(R.id.otp);

        FirebaseApp.initializeApp(LoginActivity.this);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        token = task.getResult();

                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("token", token);
                        //    Toast.makeText(LoginActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });

        hitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phone.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please Enter Contact Number", Toast.LENGTH_SHORT).show();
                } else {
                    if (secondTime.equals("false")) {
                        showProgressDialog();
                        loginUser();
                    } else {
                        if (otp.getText().toString().length() > 0) {
                            showProgressDialog();
                            loginUserWithOTP();
                        } else {
                            Toast.makeText(LoginActivity.this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        signUp = findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("device_token", token);
                startActivity(intent);
            }
        });

        initialseGoogle();
        initialiseFB();

        signOutBoth();


    }

    public void initialiseFB() {

        // FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        loginButtonFB.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    SocialUserid = object.getString("id");
                                    SocialFirstname = object.getString("first_name");
                                    SocialLastname = object.getString("last_name");
                                    if (object.has("gender")) {
                                        gender = object.getString("gender");
                                    } else {
                                        gender = "";
                                    }
                                    if (object.has("birthday")) {
                                        birthday = object.getString("gender");
                                    } else {
                                        birthday = "";
                                    }

                                    if (object.has("email")) {
                                        SocialEmailid = object.getString("email");
                                    } else {
                                        SocialEmailid = "";
                                    }


                                    social_userID = SocialUserid;
                                    social_userName = SocialFirstname + " " + SocialLastname;

                                    SocialType = "facebook";

                                    showProgressDialog();
                                    socialLogin(social_userID, social_userName, "FACEBOOK");

                                    Log.e("Social_FB", SocialFirstname + " , " + SocialEmailid + " , " + SocialUserid);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email "); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
                facebook.setVisibility(View.VISIBLE);

            }

            @Override
            public void onError(FacebookException exception) {
                facebook.setVisibility(View.VISIBLE);
                // App code
                Toast.makeText(getApplicationContext(), exception.getMessage().toString(), Toast.LENGTH_SHORT).show();
                Log.v("LoginActivity", exception.getMessage().toString());
                Log.v("LoginActivity", "error");
            }
        });
    }

    public void initialseGoogle() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext()).
                //       enableAutoManage(this, this).
                        addApi(Auth.GOOGLE_SIGN_IN_API, gso).
                        build();

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            acct = result.getSignInAccount();

            SocialUserid = acct.getId();
            SocialFirstname = acct.getGivenName();
            SocialLastname = acct.getFamilyName();
            SocialEmailid = acct.getEmail();
            SocialType = "google";

            social_userID = SocialUserid;
            social_userName = SocialFirstname + " " + SocialLastname;

            showProgressDialog();
            socialLogin(social_userID, social_userName, "GOOGLE");

            Log.e("Social_Goog", SocialFirstname + " , " + SocialEmailid + "   ,  " + SocialUserid);

        } else {
            Log.e("Social_Goog", "Failed" + result.getStatus().getStatusMessage());
        }
    }

    public void signOutBoth() {

        //For Facebook
        LoginManager.getInstance().logOut();
        //    For Google

        GoogleSignInOptions gso = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                build();

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(LoginActivity.this, gso);
        googleSignInClient.signOut();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    public void loginUser() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", phone.getText().toString());
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.SEND_OTP, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        secondTime = "true";
                        try {
                            String status = response.getString("status");

                            if (status.equalsIgnoreCase("0")) {
                                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                                intent.putExtra("phoneNumber", phone.getText().toString());
                                intent.putExtra("device_token", token);
                                startActivity(intent);
                            } else if (status.equalsIgnoreCase("1")) {
                                String otpp = response.getString("otp");

                                String message = response.getString("message");
                                otp.setText(otpp);
                                //      Toast.makeText(LoginActivity.this, "" + message + "OTP is: " + otpp, Toast.LENGTH_SHORT).show();
                                otp.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            hideDialog();
                        }
                        Log.d(TAG, response.toString() + "Add responce");

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "error11");

                        hideDialog();

                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }

                    }

                }) {
        };

        Mysingleton.getInstance(LoginActivity.this).addTorequestque(jsonObjReq);
    }

    public void loginUserWithOTP() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", phone.getText().toString());
            jsonObject.put("otp", otp.getText().toString());
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.LOGIN_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        secondTime = "true";
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");

                            if (status.equalsIgnoreCase("1")) {

                                JSONObject dataObject = response.getJSONObject("data");

                                String id = dataObject.getString("id");

                                //   String type_user_id = dataObject.getString("type_user_id");

                                String name = dataObject.getString("name");
                                String email = dataObject.getString("email");
                                String age = dataObject.getString("age");

                                String phone = dataObject.getString("phone");
                                String token = dataObject.getString("token");
                                String gender = dataObject.getString("gender");
                                /*String otpp = response.getString("otp");
                                Toast.makeText(LoginActivity.this, otpp, Toast.LENGTH_SHORT).show();
*/

                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_ID, id);
                                //     SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_USER_ID, type_user_id);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_NAME, name);

                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_EMAIL, email);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_AGE, age);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_PHONE, phone);

                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_TOKEN, token);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_GENDER, gender);

                                Intent intent = new Intent(LoginActivity.this, LeaderBoardGlobActivity.class);
                                startActivity(intent);
                                finish();

                            } else {

                                Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            //  hideDialog();
                        }
                        Log.d(TAG, response.toString() + "Add responce");

                        // hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "error11");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(LoginActivity.this).addTorequestque(jsonObjReq);
    }


    public void socialLogin(String socialID, String socialName, String socialType) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("social_id", socialID);
            jsonObject.put("name", socialName);

            Log.d("Social_FB", jsonObject.toString() + "Add Params");
        } catch (Exception e) {

        }


        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.SOCIAL_CHECK, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("Social_FB", response.toString() + "Add responce");

                        hideDialog();

                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            String responseCode = response.getString("responseCode");

                            Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            if (responseCode.equalsIgnoreCase("0001")) {

                                JSONObject dataObject = response.getJSONObject("data");

                                String type_user_id = "";

                                String id = dataObject.getString("id");

                                if (dataObject.has("type_user_id")) {
                                    type_user_id = dataObject.getString("type_user_id");
                                }

                                String name = dataObject.getString("name");
                                String email = dataObject.getString("email");
                                String age = dataObject.getString("age");
                                String phone = dataObject.getString("phone");
                                String token = dataObject.getString("token");
                                String gender = dataObject.getString("gender");

                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_ID, id);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_USER_ID, type_user_id);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_NAME, name);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_EMAIL, email);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_AGE, age);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_PHONE, phone);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_TOKEN, token);
                                SharedpreferenceUtility.getInstance(LoginActivity.this).setUser(SAVE_GENDER, gender);

                                Intent intent = new Intent(LoginActivity.this, NavDashActivity.class);
                                startActivity(intent);

                            } else {
                                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);

                                intent.putExtra("userId", social_userID);
                                intent.putExtra("social_type", socialType);
                                intent.putExtra("userName", social_userName);
                                intent.putExtra("device_token", token);

                                startActivity(intent);

                                Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            //  hideDialog();
                            Log.d("Social_FB", e.getMessage() + "Add responce");
                        }

                        Log.d("Social_FB", response.toString() + "Add responce");
                        // hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Social_FB", error.getLocalizedMessage() + "error11");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Log.d("Social_FB", message + "--- error11");
                            Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(LoginActivity.this).addTorequestque(jsonObjReq);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
