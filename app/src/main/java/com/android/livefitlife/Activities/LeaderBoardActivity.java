package com.android.livefitlife.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.ActivitySelectionAdapter;
import com.android.livefitlife.Adapters.LevelListAdapter;
import com.android.livefitlife.Adapters.UserLevelListCardAdapter;
import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.Models.LevelModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Adapters.UserListCardAdapter;
import com.android.livefitlife.Models.UserModel;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.livefitlife.VideoCalling.Dialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.jem.fliptabs.FlipTab;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class LeaderBoardActivity extends BaseActivity {
    RecyclerView listUserTop;
    RecyclerView listUserLevel;
    ArrayList<UserModel> arrayList;
    ArrayList<UserModel> arrayList2;
    CardView btnHome;
    FlipTab flipTabs;
    ArrayList<LevelModel> levelList;
    RecyclerView recyclerView;
    BottomSheetDialog dialog;
    ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaeder_board_user);

        back_button = findViewById(R.id.back_button);
        flipTabs = findViewById(R.id.flipTabs);
        listUserTop = findViewById(R.id.listUserTop);
        listUserTop.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        listUserLevel = findViewById(R.id.listUserLevl);
        listUserLevel.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        arrayList = new ArrayList<>();

        UserListCardAdapter userListAdapter = new UserListCardAdapter(getApplicationContext(), arrayList);
        listUserTop.setAdapter(userListAdapter);

        flipTabs.setTabSelectedListener(new FlipTab.TabSelectedListener() {
            @Override
            public void onTabSelected(boolean isLeftTab, @NotNull String s) {
                if (isLeftTab) {
                    listUserTop.setVisibility(View.VISIBLE);
                    listUserLevel.setVisibility(View.GONE);
                    UserListCardAdapter userListAdapter = new UserListCardAdapter(getApplicationContext(), arrayList);
                    listUserTop.setAdapter(userListAdapter);
                } else {
                    listUserTop.setVisibility(View.GONE);

                    getLevelList();

                }
            }

            @Override
            public void onTabReselected(boolean b, @NotNull String s) {

            }
        });

    }

    public void showDialogLevelList() {
        View view = getLayoutInflater().inflate(R.layout.dialoglevellist, null);

        dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        recyclerView = dialog.findViewById(R.id.levelList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        arrayList2 = new ArrayList<>();


        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void getLevelList() {
        showProgressDialog();
        levelList = new ArrayList<>();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_LEVEL_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showDialogLevelList();
                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");


                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                String levelName = dataObject.getString("name");
                                String levelId = dataObject.getString("id");

                                LevelModel levelModel = new LevelModel();
                                levelModel.setLevelId(levelId);
                                levelModel.setLevelName(levelName);
                                levelList.add(levelModel);

                                if (i == 0) {
                                    //   getWorkoutVideos(levelId);
                                }
                            }

                            LevelListAdapter listAdapter = new LevelListAdapter(LeaderBoardActivity.this, levelList, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {
                                    String levelId = levelList.get(position).getLevelId();
                                    listUserLevel.setVisibility(View.VISIBLE);
                                    UserLevelListCardAdapter userlevelListAdapter = new UserLevelListCardAdapter(getApplicationContext(), arrayList2);
                                    listUserLevel.setAdapter(userlevelListAdapter);
                                    dialog.dismiss();

                                }
                            });

                            recyclerView.setAdapter(listAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(LeaderBoardActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(LeaderBoardActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(LeaderBoardActivity.this).addTorequestque(jsonObjReq);
    }

}
