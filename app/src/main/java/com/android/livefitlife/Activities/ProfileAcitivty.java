package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.R;
import com.android.livefitlife.SubscriptionActivity;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.alterac.blurkit.BlurLayout;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_AGE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_EMAIL;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_HEIGHT;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_IMAGE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_NAME;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_SUBSCRIPTION_STATUS;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_WEIGHT;

public class ProfileAcitivty extends BaseActivity {

    TextView ranking, totalpoints, userName, email, height, weight, age;
    CardView editProfile;
    CircleImageView imagenon;
    RelativeLayout upgradeLayout, upgradePremiumLayout;
    BlurLayout blurLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_acitivty);

        editProfile = findViewById(R.id.editProfile);

        imagenon = findViewById(R.id.imagenon);
        userName = findViewById(R.id.userName);
        email = findViewById(R.id.email);
        age = findViewById(R.id.age);
        height = findViewById(R.id.height);
        weight = findViewById(R.id.weight);
        upgradeLayout = findViewById(R.id.upgradeLayout);
        upgradePremiumLayout = findViewById(R.id.upgradePremiumLayout);

        blurLayout = findViewById(R.id.blurLayout);

        String premium_status = SharedpreferenceUtility.getInstance(getApplicationContext()).getUser(SAVE_SUBSCRIPTION_STATUS);

        getPremiumProfile(premium_status);

        blurLayout.setBlurRadius(15);
        blurLayout.setDownscaleFactor(0.5F);
        blurLayout.setFPS(0);
        blurLayout.setCornerRadius(1F);
        blurLayout.startBlur();

        getProfile();

        upgradeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivitiesList();
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileAcitivty.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });

        ranking = findViewById(R.id.ranking);
        totalpoints = findViewById(R.id.totalPoints);

     //   leaderBoard = findViewById(R.id.leaderBoard);
     /*   leaderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileAcitivty.this, LeaderBoardActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        String rank = "<font color=#BC0E83> #30 </font> <font color=#000> Rank in Ranking </font>";
        ranking.setText(Html.fromHtml(rank));

        String points = "<font color=#BC0E83> 65% </font> <font color=#000> Goal Progress </font>";
        totalpoints.setText(Html.fromHtml(points));

    }

    private void getPremiumProfile(String premium_status) {
        if (premium_status.equals("active")) {
            upgradePremiumLayout.setVisibility(View.GONE);
        } else {
            upgradePremiumLayout.setVisibility(View.VISIBLE);


        }
    }

    public void getActivitiesList() {
        showProgressDialog();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");


                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                ActivityModel activityModel = new ActivityModel();
                                activityModel.setActivityId(dataObject.getString("id"));
                                String isLFL = dataObject.getString("is_lfl");
                                activityModel.setDescription(dataObject.getString("description"));
                                activityModel.setActivityName(dataObject.getString("name"));
                                activityModel.setActivityIcon(dataObject.getString("image"));

                                if (isLFL.equals("1")) {
                                    Intent intent = new Intent(ProfileAcitivty.this, SubscriptionActivity.class);
                                    intent.putExtra("categoryID", dataObject.getString("id"));
                                    intent.putExtra("trainingType", "");
                                    startActivity(intent);
                                }

                            }


                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(ProfileAcitivty.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(ProfileAcitivty.this).addTorequestque(jsonObjReq);

    }

    public void getProfile() {

        showProgressDialog();
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_PROFILE, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("Response", response.toString());
                        hideDialog();
                        try {
                            JSONObject dataObject = response.getJSONObject("data");

                            email.setText(dataObject.getString("email"));
                            SharedpreferenceUtility.getInstance(ProfileAcitivty.this).setUser(SAVE_EMAIL, dataObject.getString("email"));
                            userName.setText(dataObject.getString("name"));
                            SharedpreferenceUtility.getInstance(ProfileAcitivty.this).setUser(SAVE_NAME, dataObject.getString("name"));
                            age.setText("Age: " + dataObject.getString("age"));
                            SharedpreferenceUtility.getInstance(ProfileAcitivty.this).setUser(SAVE_AGE, dataObject.getString("age"));
                            height.setText("Height: " + dataObject.getString("height"));
                            SharedpreferenceUtility.getInstance(ProfileAcitivty.this).setUser(SAVE_HEIGHT, dataObject.getString("height"));
                            weight.setText("Weight: " + dataObject.getString("weight"));
                            SharedpreferenceUtility.getInstance(ProfileAcitivty.this).setUser(SAVE_WEIGHT, dataObject.getString("weight"));


                            String imagefilePath = dataObject.getString("image");
                            SharedpreferenceUtility.getInstance(ProfileAcitivty.this).setUser(SAVE_IMAGE, dataObject.getString("image"));

                            if (imagefilePath != null) {
                                imagenon.setBorderColor(getResources().getColor(R.color.orangetheme));
                                Picasso.get().load(imagefilePath).placeholder(R.drawable.defaultt).into(imagenon);
                            }

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);

                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(ProfileAcitivty.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(ProfileAcitivty.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(ProfileAcitivty.this).addTorequestque(jsonObjReq);
    }

}
