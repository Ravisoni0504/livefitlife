package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.ActivityAdapter;
import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.SpaceItem;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class ActivitiesGymActivity extends BaseActivity {
    RecyclerView activtiesListssss;
    ArrayList<ActivityModel> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities_gym);

        activtiesListssss = findViewById(R.id.activtiesList);
        activtiesListssss.setLayoutManager(new GridLayoutManager(this, 2));

        arrayList = new ArrayList<>();

      /*  ActivityAdapter adapter = new ActivityAdapter(this, arrayList, new OnClickActivity() {
            @Override
            public void OnClickActivity(int position) {



            }
        });
        activtiesListssss.setAdapter(adapter);
*/
        ImageView back_button = findViewById(R.id.back_button);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //getActivities();
        showProgressDialog();
        getActivitiesList();
    }

    public void getActivities() {

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        Log.e("dataActiviy", response.toString());
                        try {

                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i <= data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                ActivityModel activityModel = new ActivityModel();
                                activityModel.setActivityName(dataObject.getString("name"));
                                activityModel.setActivityIcon(dataObject.getString("image"));
                                arrayList.add(activityModel);

                            }

                            ActivityAdapter activityAdapter = new ActivityAdapter(ActivitiesGymActivity.this, arrayList, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {

                                }
                            });

                            activtiesListssss.setAdapter(activityAdapter);

                        } catch (Exception e) {
                            Log.e("dataActiviyException", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(ActivitiesGymActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {
        };
        Mysingleton.getInstance(ActivitiesGymActivity.this).addTorequestque(jsonObjReq);
    }

    public void getActivitiesList() {

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                ActivityModel activityModel = new ActivityModel();
                                activityModel.setActivityId(dataObject.getString("id"));
//                                Toast.makeText(ActivitiesGymActivity.this, "" + arrayList.get(position).getId(), Toast.LENGTH_SHORT).show();
                                activityModel.setActivityName(dataObject.getString("name"));
                                activityModel.setActivityIcon(dataObject.getString("image"));

                                arrayList.add(activityModel);

                            }

                            ActivityAdapter activityAdapter = new ActivityAdapter(ActivitiesGymActivity.this, arrayList, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {

                                    Intent intent = new Intent(ActivitiesGymActivity.this, ActivitiesDetailActivity.class);
                                    intent.putExtra("id", arrayList.get(position).getActivityId());
                                    startActivity(intent);

                                    Toast.makeText(ActivitiesGymActivity.this, "" + arrayList.get(position).getActivityId(), Toast.LENGTH_SHORT).show();

                                }
                            });
                            int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);

                            activtiesListssss.addItemDecoration(new SpaceItem(spacingInPixels));
                            activtiesListssss.setAdapter(activityAdapter);
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(ActivitiesGymActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(ActivitiesGymActivity.this).addTorequestque(jsonObjReq);

    }
}
