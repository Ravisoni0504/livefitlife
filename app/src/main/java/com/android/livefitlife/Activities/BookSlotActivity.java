package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.SlotAdapter;
import com.android.livefitlife.Models.SlotModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnSlotClick;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class BookSlotActivity extends BaseActivity {

    Button sbmitButton;

    ArrayList<SlotModel> arrayList;
    RecyclerView slotViewss;
    String selectedSlot = "";
    String dateSelected = "";
    SlotAdapter adapter;
    String categoryId;

    String dayStr = "";
    String monthStr = "";
    String yearStr = "";
    // DatePickerTimeline datePickerTimeline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_slot);

        categoryId = getIntent().getStringExtra("categoryId");

        sbmitButton = findViewById(R.id.sbmitButton);

        sbmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selectedSlot.equals("")) {
                    BookSlot();
                } else {
                    Toast.makeText(BookSlotActivity.this, " Please Select Slot and Start Date  ", Toast.LENGTH_SHORT).show();
                }
            }
        });

        slotViewss = findViewById(R.id.slotView);
        slotViewss.setLayoutManager(new GridLayoutManager(this, 3));

        getSlots();

    }

    public void getSlots() {

        showProgressDialog();

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("category_id", categoryId);

        } catch (Exception e) {

        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.SLOT_GET, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        try {
                            JSONArray data = response.getJSONArray("data");

                            Log.e("zumbaSlot", data.toString());

                            arrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                SlotModel dietChartModel = new SlotModel();
                                String slotTime = splitTime(dataObject.getString("start_time"));
                                dietChartModel.setTime(slotTime);
                                dietChartModel.setId(dataObject.getString("id"));
                                dietChartModel.setCategoryId(dataObject.getString("category_id"));
                                arrayList.add(dietChartModel);

                            }

                            adapter = new SlotAdapter(BookSlotActivity.this, arrayList, new OnSlotClick() {
                                @Override
                                public void onSlotTap(int position) {

                                    selectedSlot = arrayList.get(position).getId();

                                    final Calendar c = Calendar.getInstance();
                                    int year = c.get(Calendar.YEAR);
                                    int month = c.get(Calendar.MONTH);
                                    int date = c.get(Calendar.DAY_OF_MONTH);

                                    int monthInt = month + 1;
                                    int dayInt = date;

                                    if (monthInt < 10) {
                                        monthStr = "0" + monthInt;
                                    } else {
                                        monthStr = monthInt + "";
                                    }

                                    if (dayInt < 10) {
                                        dayStr = "0" + dayInt;
                                    } else {
                                        dayStr = dayInt + "";
                                    }

                                    yearStr = year + "";

                                    Toast.makeText(BookSlotActivity.this, year + "---" + (month + 1) + "---" + date, Toast.LENGTH_SHORT).show();

                                  /*  datePickerTimeline.setInitialDate(year, month, date);
                                    datePickerTimeline.setDateTextColor(getResources().getColor(R.color.orangetheme));
                                    datePickerTimeline.setDayTextColor(getResources().getColor(R.color.orangetheme));
                                    datePickerTimeline.setMonthTextColor(getResources().getColor(R.color.orangethemeTra));
                                    // Set a date Selected Listener
                                    datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
                                        @Override
                                        public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                                            // Do Something
                                            //    Toast.makeText(BookSlotActivity.this, ""+year + "-" + (month + 1) + "-" + day, Toast.LENGTH_SHORT).show();

                                            String monthStr = "";
                                            String dayStr = "";
                                            int monthInt = month + 1;
                                            int dayInt = day;

                                            if (monthInt < 10) {
                                                monthStr = "0" + monthInt;
                                            } else {
                                                monthStr = monthInt + "";
                                            }

                                            if (dayInt < 10) {
                                                dayStr = "0" + dayInt;
                                            } else {
                                                dayStr = dayInt + "";
                                            }

                                            dateSelected = year + "-" + monthStr + "-" + dayStr + " 00:00:00";

                                        }

                                        @Override
                                        public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                                            // Do Something
                                        }
                                    });*/
                                }
                            });
                            slotViewss.setAdapter(adapter);

                            Log.d("zumbaSlot", response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d("zumbaSlot", response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("zumbaSlot", error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(BookSlotActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(BookSlotActivity.this).getUser(SAVE_TOKEN));
                Log.e("zumbaSlot", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(BookSlotActivity.this).addTorequestque(jsonObjReq);
    }


    public void BookSlot() {

        showProgressDialog();

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("category_id", categoryId);
            jsonObject.put("start_date", yearStr + "-" + monthStr + "-" + dayStr + " 00:00:00");
            jsonObject.put("slot_id", selectedSlot);

            Log.d("dataOrder", jsonObject.toString());
        } catch (Exception e) {

        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.BOOK_SLOT, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        try {

                            if (response.getString("status").equals("1")) {

                                String message = response.getString("message");
                                Toast.makeText(BookSlotActivity.this, "" + message, Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(BookSlotActivity.this, NavDashActivity.class);
                                startActivity(intent);
                                finish();

                            }

                        } catch (Exception e) {

                        }
                        Toast.makeText(BookSlotActivity.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("dataOrder", response.toString() + "--- Add responce  ----" + response);

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("dataOrder", error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");
                            Log.d("dataOrder", message + "responce");
                            Toast.makeText(BookSlotActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(BookSlotActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(BookSlotActivity.this).addTorequestque(jsonObjReq);
    }


    public String splitTime(String timeActual) {
        String concattedStr = "";
        String[] splitted = timeActual.split(":");
        int hour = Integer.parseInt(splitted[0]);
        //  int minute = Integer.parseInt(splitted[1]);
        if (hour >= 12) {
            concattedStr = "PM";
            hour = hour - 12;

        } else {
            concattedStr = "AM";
        }


        String newSlot = hour + ":" + splitted[1] + " " + concattedStr;
        return newSlot;
    }
}
