package com.android.livefitlife.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.DietChartAdapter;
import com.android.livefitlife.Models.DietChartModel;
import com.android.livefitlife.R;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickDietPlan;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class DietChartActivity extends BaseActivity {
    RecyclerView dietChart;
    ArrayList<DietChartModel> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_chart);

        dietChart = findViewById(R.id.dietChart);
        dietChart.setLayoutManager(new LinearLayoutManager(this));


        getChartList();
    }

    public void showDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialogdiet, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        CardView vegTab = dialog.findViewById(R.id.vegTab);
        vegTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        CardView nonvegTab = dialog.findViewById(R.id.nonvegTab);
    }

    public void openPDF(String path) {

        //   String pdf_url = "https://drive.google.com/file/d/1j3e1fUJnvwJT9bNaamKxBPWMgelIPbCO/view";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        startActivity(browserIntent);

    }


    public void getChartList() {

        showProgressDialog();
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_DIET_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            arrayList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                DietChartModel dietChartModel = new DietChartModel();
                                dietChartModel.setDietChartPath(dataObject.getString("vegeterian"));
                                dietChartModel.setDietName(dataObject.getString("name"));
                                dietChartModel.setLevelName(dataObject.getString("level_name"));
                                dietChartModel.setLevelId(dataObject.getString("id"));
                                arrayList.add(dietChartModel);

                            }

                            DietChartAdapter dietChartAdapter = new DietChartAdapter(DietChartActivity.this, arrayList, new OnClickDietPlan() {
                                @Override
                                public void OnClickDiet(int position) {
                                    //  showDialog();
                                    openPDF(arrayList.get(position).getDietChartPath());
                                }
                            });

                            dietChart.setAdapter(dietChartAdapter);


                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(DietChartActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(DietChartActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(DietChartActivity.this).addTorequestque(jsonObjReq);
    }
}
