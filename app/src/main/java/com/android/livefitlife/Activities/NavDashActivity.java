package com.android.livefitlife.Activities;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.ActivitySelectionAdapter;
import com.android.livefitlife.Adapters.MenuListAdapter;
import com.android.livefitlife.Models.ActivityModel;
import com.android.livefitlife.Models.MenuModelMain;
import com.android.livefitlife.Notification.MyListener;
import com.android.livefitlife.R;
import com.android.livefitlife.SubscriptionActivity;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.livefitlife.roomdb.SubscriptionModelRoom;
import com.android.livefitlife.roomdb.UtilClass;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.infideap.drawerbehavior.Advance3DDrawerLayout;
import com.squareup.picasso.Picasso;
import com.vivekkaushik.datepicker.DatePickerTimeline;
import com.vivekkaushik.datepicker.OnDateSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.livefitlife.Utility.SharedpreferenceUtility.LOCAL_TABLE_STATUS;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_EMAIL;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_IMAGE;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_NAME;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_SUBSCRIPTION_STATUS;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class NavDashActivity extends BaseActivity implements MyListener {

    private static final String TAG = NavDashActivity.class.getSimpleName();
    ArrayList<MenuModelMain> menuList;
    ImageView requestDemo, menuButton;
    // String videoAccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzY2MDZkYTNmNTI0MzY4YmQxNjc3OTRmZTFlYjYzNDY2LTE2MDg3MjI1MTQiLCJpc3MiOiJTSzY2MDZkYTNmNTI0MzY4YmQxNjc3OTRmZTFlYjYzNDY2Iiwic3ViIjoiQUM1OGQ4MjQ4OTY1NmIzMjU4MGU3YjE2YmVmYTRmYzYwNiIsImV4cCI6MTYwODcyNjExNCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoicmF2aSBzb25pIiwidmlkZW8iOnsicm9vbSI6InRlc3QifX19.UueNss4lKULsR3gCNPBaq8RT2n86z-gl87FoJMAW_2I";
    private List<ActivityModel> listActivity;
    private ArrayList<SubscriptionModelRoom> subsCriptionList;
    ActivitySelectionAdapter activitySelectionAdapter;
    RelativeLayout scheduleCard;
    ImageView imagenon;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private final static String default_notification_channel_id = "default";
    TextView dashboard;
    ImageView notificationButton;
    RecyclerView activityList;
    DatePickerTimeline datePickerTimeline;
    String levelId, time, date, slotTime;
    TextView top;
    RelativeLayout dietPlanLay, pendingLay;
    RelativeLayout main;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_dash);

        final Advance3DDrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        drawer.setViewRotation(GravityCompat.START, 15);
        RecyclerView listMenu = navigationView.findViewById(R.id.listMenu);
        listMenu.setLayoutManager(new LinearLayoutManager(this));

        menuItem();

        notificationButton = findViewById(R.id.notificationButton);
        notificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDashActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });

        dashboard = findViewById(R.id.dashboard);

        TextView name = navigationView.findViewById(R.id.name);
        TextView email = navigationView.findViewById(R.id.email);

        imagenon = navigationView.findViewById(R.id.imagenon);
        String image = SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(SAVE_IMAGE);
        if (image != null) {
            Picasso.get().load(image).placeholder(R.drawable.defaultt).into(imagenon);
        }

        name.setText(SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(SAVE_NAME));
        email.setText(SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(SAVE_EMAIL));

        menuButton = findViewById(R.id.menuButton);
        scheduleCard = findViewById(R.id.scheduleCard);

        scheduleCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TodayTaskActivity.class);
                intent.putExtra("BY", "Dashboard");
                startActivity(intent);
            }
        });

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.open();
            }
        });

        requestDemo = findViewById(R.id.requestDemo);
        requestDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRequestDemoStatus();
            }
        });

        RelativeLayout workoutCard = findViewById(R.id.workoutCard);
        workoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDashActivity.this, WorkOutActivityList.class);
                startActivity(intent);
            }
        });

        RelativeLayout dietrecipe = findViewById(R.id.dietrecipe);
        dietrecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDashActivity.this, DietReceipeActivity.class);
                startActivity(intent);
            }
        });

        RelativeLayout profileCard = findViewById(R.id.profileCard);
        profileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDashActivity.this, ProfileAcitivty.class);
                startActivity(intent);
            }
        });

        RelativeLayout membershipCard = findViewById(R.id.membershipCard);
        membershipCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDashActivity.this, SubscriptionActivity.class);
                startActivity(intent);
            }
        });

        dietPlanLay = findViewById(R.id.dietPlanLay);
        dietPlanLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String premium_status = SharedpreferenceUtility.getInstance(getApplicationContext()).getUser(SAVE_SUBSCRIPTION_STATUS);
                getPremiumProfile(premium_status);
            }
        });

        MenuListAdapter menuListAdapter = new MenuListAdapter(this, menuList);
        listMenu.setAdapter(menuListAdapter);

        getListDataAll();
    }

    private void getPremiumProfile(String premium_status) {
        if (premium_status.equals("active")) {
            Intent intent = new Intent(NavDashActivity.this, DietChartActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Diet Plan only for Premium Users", Toast.LENGTH_SHORT).show();
        }
    }

    public void generateNotification() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(NavDashActivity.this, default_notification_channel_id);
        mBuilder.setContentTitle("My Notification");
        mBuilder.setContentText("Notification Listener Service Example");
        mBuilder.setTicker("Notification Listener Service Example");
        mBuilder.setSmallIcon(R.drawable.ic_launcher_foreground);
        mBuilder.setAutoCancel(true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_dash, menu);
        return true;
    }

    public void menuItem() {
        menuList = new ArrayList<>();

        MenuModelMain menuModelMain = new MenuModelMain();
        menuModelMain.setMenuName("Home");
        menuModelMain.setMenuIcon(R.drawable.home_button);
        menuList.add(menuModelMain);

        MenuModelMain mysubscription = new MenuModelMain();
        mysubscription.setMenuName("My Subscription");
        mysubscription.setMenuIcon(R.drawable.question_us);
        menuList.add(mysubscription);

        MenuModelMain leaderboard = new MenuModelMain();
        leaderboard.setMenuName("Leader Board");
        leaderboard.setMenuIcon(R.drawable.question_us);
        menuList.add(leaderboard);

        MenuModelMain contactus = new MenuModelMain();
        contactus.setMenuName("Contact Us");
        contactus.setMenuIcon(R.drawable.contact_us);
        menuList.add(contactus);

        MenuModelMain logout = new MenuModelMain();
        logout.setMenuName("Logout");
        logout.setMenuIcon(R.drawable.login_button);
        menuList.add(logout);
    }

    public void showDialogRequest() {
        View view = getLayoutInflater().inflate(R.layout.dialogrequestsession, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        CardView demoTab = dialog.findViewById(R.id.demoTab);
        demoTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogDemoOrTest();
                dialog.dismiss();
            }
        });

        CardView testTab = dialog.findViewById(R.id.testTab);

        testTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogTest();
                dialog.dismiss();
            }
        });
    }

    public void showDialogTest() {

        View view = getLayoutInflater().inflate(R.layout.request_test, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        DatePickerTimeline datePickerTimeline = dialog.findViewById(R.id.datePickerTimeline);
        // Set a Start date (Default, 1 Jan 1970)
        Calendar calendar = Calendar.getInstance();

        int date = calendar.DATE;
        int month = calendar.MONTH;
        int year = calendar.YEAR;

        datePickerTimeline.setInitialDate(year, month, date);
        datePickerTimeline.setDateTextColor(getResources().getColor(R.color.orangetheme));
        datePickerTimeline.setDayTextColor(getResources().getColor(R.color.orangetheme));
        datePickerTimeline.setMonthTextColor(getResources().getColor(R.color.orangethemeTra));

        final LinearLayout timeCard = dialog.findViewById(R.id.timeCard);
        timeCard.setVisibility(View.GONE);

        final TextView hourTv = dialog.findViewById(R.id.hourTv);
        final TextView minuteTv = dialog.findViewById(R.id.minuteTv);
        final TextView am_time = dialog.findViewById(R.id.am_time);
        final TextView pm_time = dialog.findViewById(R.id.pm_time);

        // Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                // Do Something
                onDateSelection(hourTv, minuteTv, am_time, pm_time, timeCard);
            }

            @Override
            public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                // Do Something
            }
        });

    }

    public void showDialogDemoStatus() {
        View view = getLayoutInflater().inflate(R.layout.dialogrequestdemostatus, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        pendingLay = dialog.findViewById(R.id.pendingLay);

        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void showDialogDemoOrTest() {
        View view = getLayoutInflater().inflate(R.layout.dialogrequestdemo, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        top = dialog.findViewById(R.id.top);
        main = dialog.findViewById(R.id.main);
        //  pendingLay = dialog.findViewById(R.id.pendingLay);

        submit = dialog.findViewById(R.id.submitQuery);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (date.equals("null") && time.equals("null")) {
                    Toast.makeText(getApplicationContext(), "Select date and time for demo request", Toast.LENGTH_SHORT).show();
                } else {
                    postRequestDemo(dialog);
                }
            }
        });

        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        showProgressDialog();
        datePickerTimeline = dialog.findViewById(R.id.datePickerTimeline);

        activityList = dialog.findViewById(R.id.activityList);
        activityList.setLayoutManager(new LinearLayoutManager(this));


        listActivity = new ArrayList<>();

        getActivitiesList();

        final LinearLayout timeCard = dialog.findViewById(R.id.timeCard);

        timeCard.setVisibility(View.GONE);

        final TextView hourTv = dialog.findViewById(R.id.hourTv);
        final TextView minuteTv = dialog.findViewById(R.id.minuteTv);
        final TextView am_time = dialog.findViewById(R.id.am_time);
        final TextView pm_time = dialog.findViewById(R.id.pm_time);

        // Set a Start date (Default, 1 Jan 1970)
        datePickerTimeline.setInitialDate(2020, 10, 25);
        datePickerTimeline.setDateTextColor(getResources().getColor(R.color.orangetheme));
        datePickerTimeline.setDayTextColor(getResources().getColor(R.color.orangetheme));
        datePickerTimeline.setMonthTextColor(getResources().getColor(R.color.orangethemeTra));

        // Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                // Do Something
                onDateSelection(hourTv, minuteTv, am_time, pm_time, timeCard);
                date = String.valueOf(year) + "-" + (month + 1) + "-" + day;
            }

            @Override
            public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                // Do Something
            }
        });

        datePickerTimeline.setVisibility(View.GONE);
    }

    public void onDateSelection(final TextView hourTV, final TextView minuteTV, final TextView am_time, final TextView pm_time, final LinearLayout timeCard) {
        Calendar mcurrentTime = Calendar.getInstance();

        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(NavDashActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                timeCard.setVisibility(View.VISIBLE);

                hourTV.setText(selectedHour + "");
                minuteTV.setText(selectedMinute + "");

                time = selectedHour + ":" + selectedMinute;

                if (selectedHour > 12) {

                    am_time.setText("PM");
                    pm_time.setText("AM");

                } else {

                    am_time.setText("AM");
                    pm_time.setText("PM");
                }

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @Override
    public void setValue(String packageName) {
        dashboard.setText(packageName);

        generateNotification();
    }

    public void getSubscription() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_SUBSCRIPTION, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("responseSub", response.toString());

                        try {

                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {

                                JSONObject jsonObject = data.getJSONObject(i);

                                String status = jsonObject.getString("status");
                                String category_id = jsonObject.getString("category_id");

                                if (status.equals("active")) {
                                    UtilClass.updateActivityStatus(NavDashActivity.this, "1", category_id);
                                } else {
                                    UtilClass.updateActivityStatus(NavDashActivity.this, "0", category_id);
                                }
                            }

                        } catch (Exception e) {
                            Log.e("responseSub", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //   hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");
                            Log.e("responseSub", message);
                            Toast.makeText(NavDashActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(NavDashActivity.this).addTorequestque(jsonObjReq);
    }

    public void getRequestDemoStatus() {

        showProgressDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_REQUEST_DEMO_STATUS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("response", response.toString());

                        hideDialog();
                        try {
                            String status = response.getString("status");

                            if (status.equals("1")) {
                                showDialogDemoStatus();
                                Log.e("statuss", "pending");
                            } else {
                                showDialogDemoOrTest();
                            }

                        } catch (Exception e) {
                            Log.e("orderId", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        showDialogDemoOrTest();

                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(NavDashActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(NavDashActivity.this).addTorequestque(jsonObjReq);
    }

    public void getListDataAll() {

        showProgressDialog();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        try {

                            JSONArray data = response.getJSONArray("data");

                            subsCriptionList = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);
                                SubscriptionModelRoom activityModel = new SubscriptionModelRoom();
                                activityModel.setActiId(dataObject.getString("id"));
                                activityModel.setActivityname(dataObject.getString("name"));
                                activityModel.setStatus("0");

                                subsCriptionList.add(activityModel);
                            }

                            String status = SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(LOCAL_TABLE_STATUS);
                            if (status.equals("0")) {
                                UtilClass.insertData(NavDashActivity.this, subsCriptionList);
                                SharedpreferenceUtility.getInstance(NavDashActivity.this).setUser(LOCAL_TABLE_STATUS, "true");
                            } else if (SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(LOCAL_TABLE_STATUS).equals("true")) {
                                getSubscription();
                            }

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);

                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }

                        hideDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(NavDashActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(NavDashActivity.this).addTorequestque(jsonObjReq);

    }

    public void getActivitiesList() {
        listActivity = new ArrayList<>();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.ACTIVITIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            listActivity = new ArrayList<>();

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                ActivityModel activityModel = new ActivityModel();
                                activityModel.setActivityId(dataObject.getString("id"));
                                activityModel.setIs_lfl(dataObject.getString("is_lfl"));
                                activityModel.setDescription(dataObject.getString("description"));
                                activityModel.setActivityName(dataObject.getString("name"));
                                activityModel.setActivityIcon(dataObject.getString("image"));

                                listActivity.add(activityModel);

                            }

                            activitySelectionAdapter = new ActivitySelectionAdapter(getApplicationContext(), listActivity, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {

                                    levelId = listActivity.get(position).getActivityId();

                                    datePickerTimeline.setVisibility(View.VISIBLE);

                                    ActivityModel activityModel = listActivity.get(position);

                                    ArrayList<ActivityModel> arrayList = new ArrayList<>();
                                    arrayList.add(activityModel);

                                    ActivitySelectionAdapter adapter = new ActivitySelectionAdapter(NavDashActivity.this, arrayList);
                                    activityList.setAdapter(adapter);

                                }
                            });

                            activityList.setAdapter(activitySelectionAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(NavDashActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
        };

        Mysingleton.getInstance(NavDashActivity.this).addTorequestque(jsonObjReq);

    }


    public void postRequestDemo(Dialog dialog) {

        showProgressDialog();
        slotTime = date + " " + time;
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("category_id", levelId);
            jsonObject.put("description", "desc");
            jsonObject.put("slot_time", slotTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.POST_REQUEST_DEMO, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        Log.e("dataPlan", response.toString());
                        try {

                            String status = response.getString("status");
                            JSONObject data = response.getJSONObject("data");

                            if (status.equals("1")) {
                                Toast.makeText(getApplicationContext(), "Your demo request has been received.\nTrainer will be assigned shortly"
                                        , Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        } catch (Exception e) {
                            Log.e("dataPlanException", e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        hideDialog();
                        try {

                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(NavDashActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        } catch (Exception e) {

                        }

                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(NavDashActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };
        Mysingleton.getInstance(NavDashActivity.this).addTorequestque(jsonObjReq);
    }

}
