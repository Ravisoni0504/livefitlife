package com.android.livefitlife.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Adapters.LevelListAdapter;
import com.android.livefitlife.Adapters.WorkoutListAdapter;
import com.android.livefitlife.Models.LevelModel;
import com.android.livefitlife.Models.WorkoutModel;
import com.android.livefitlife.R;
import com.android.livefitlife.SubscriptionActivity;
import com.android.livefitlife.Utility.BaseActivity;
import com.android.livefitlife.Utility.Mysingleton;
import com.android.livefitlife.Utility.OnClickActivity;
import com.android.livefitlife.Utility.SharedpreferenceUtility;
import com.android.livefitlife.Utility.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.android.livefitlife.Utility.SharedpreferenceUtility.SAVE_TOKEN;

public class WorkoutActivity extends BaseActivity {
    RecyclerView recyclerVIewWorkout, levelBar;
    ArrayList<WorkoutModel> arrayList;
    Button premium_User;
    ArrayList<LevelModel> levelList;
    String OnlineStatus, OfflineStatus, VirtualStatus, categoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        levelBar = findViewById(R.id.levelBar);
        levelBar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        categoryId = getIntent().getStringExtra("categoryID");

        recyclerVIewWorkout = findViewById(R.id.recyclerVIewWorkout);
        recyclerVIewWorkout.setLayoutManager(new LinearLayoutManager(this));

        premium_User = findViewById(R.id.premium_User);
        premium_User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialogUpgrade();
            }
        });

        getWarmupList();
        getUserSettings();
        //   getLevels();
    }

    public void inputData() {

        WorkoutModel workoutModel = new WorkoutModel();
        workoutModel.setCardColor(getResources().getColor(R.color.colorOne));
        workoutModel.setImageSet(R.drawable.exerciseone);
        workoutModel.setExerciseName("Exercise 1");
        workoutModel.setExerciseTime("15 Times / Reps");
        workoutModel.setLikeIMage(R.drawable.play_button);
        arrayList.add(workoutModel);

        WorkoutModel wm2 = new WorkoutModel();
        wm2.setCardColor(getResources().getColor(R.color.colorTwo));
        wm2.setImageSet(R.drawable.exercisetwo);
        wm2.setExerciseName("Exercise 2");
        wm2.setLikeIMage(R.drawable.play_button);
        wm2.setExerciseTime("15 Times / Reps");
        arrayList.add(wm2);

        WorkoutModel wm3 = new WorkoutModel();
        wm3.setCardColor(getResources().getColor(R.color.colorThree));
        wm3.setImageSet(R.drawable.exercisethree);
        wm3.setExerciseName("Exercise 3");
        wm3.setLikeIMage(R.drawable.play_button);
        wm3.setExerciseTime("20 Times / Reps");
        arrayList.add(wm3);

        WorkoutModel wm4 = new WorkoutModel();
        wm4.setCardColor(getResources().getColor(R.color.colorFour));
        wm4.setImageSet(R.drawable.exercisefour);
        wm4.setExerciseName("Exercise 4");
        wm4.setLikeIMage(R.drawable.play_button);
        wm4.setExerciseTime("25 Times / Reps");
        arrayList.add(wm4);
    }

    public void getLevels() {

        levelList = new ArrayList<>();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_WORKOUT_VIDEOS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");


                            for (int i = 0; i < data.length(); i++) {

                                JSONObject dataObject = data.getJSONObject(i);

                                String levelName = dataObject.getString("name");
                                String levelId = dataObject.getString("id");

                                LevelModel levelModel = new LevelModel();
                                levelModel.setLevelId(levelId);
                                levelModel.setLevelName(levelName);
                                levelList.add(levelModel);

                                if (i == 0) {
                                    //   getWorkoutVideos(levelId);
                                }
                            }

                            LevelListAdapter listAdapter = new LevelListAdapter(WorkoutActivity.this, levelList, new OnClickActivity() {
                                @Override
                                public void OnClickActivity(int position) {
                                    String levelId = levelList.get(position).getLevelId();
                                    // getWorkoutVideos(levelId);

                                }
                            });

                            levelBar.setAdapter(listAdapter);

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(WorkoutActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(WorkoutActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(WorkoutActivity.this).addTorequestque(jsonObjReq);
    }

    public void getWarmupList() {

        showProgressDialog();
        arrayList = new ArrayList<>();

        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.GET, URLs.GET_WARMUP_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {

                                WorkoutModel workoutModel = new WorkoutModel();
                                JSONObject dataObject = data.getJSONObject(i);

                                workoutModel.setExerciseName(dataObject.getString("name"));
                                workoutModel.setDescription(dataObject.getString("description"));
                                workoutModel.setImage(dataObject.getString("image"));
                                String categoryid = dataObject.getString("id");

                                arrayList.add(workoutModel);

                                WorkoutListAdapter workoutListAdapter = new WorkoutListAdapter(WorkoutActivity.this, arrayList);
                                recyclerVIewWorkout.setAdapter(workoutListAdapter);

                            }

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(WorkoutActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(WorkoutActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(WorkoutActivity.this).addTorequestque(jsonObjReq);
    }


    public void showDialogUpgrade() {
        View view = getLayoutInflater().inflate(R.layout.upgrade_layout, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();


        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        CardView onlineTrainingCard = dialog.findViewById(R.id.onlineTrainingCard);
        onlineTrainingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OnlineStatus.equals("Y")) {
                    Intent intent = new Intent(WorkoutActivity.this, SubscriptionActivity.class);
                    intent.putExtra("categoryID", categoryId);
                    intent.putExtra("trainingType", "OnlineTraining");
                    startActivity(intent);
                    //   dialog.dismiss();
                }
            }
        });

        CardView virtualTrainingCard = dialog.findViewById(R.id.virtualTrainingCard);
        virtualTrainingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VirtualStatus.equals("Y")) {
                    Intent intent = new Intent(WorkoutActivity.this, SubscriptionActivity.class);
                    intent.putExtra("categoryID", categoryId);
                    intent.putExtra("trainingType", "VirtualTraining");
                    startActivity(intent);
                    //  dialog.dismiss();
                }

            }
        });
    }


    public void getUserSettings() {


        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, URLs.GET_USER_SETTINGS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONObject dataObject = response.getJSONObject("data");

                            OnlineStatus = dataObject.getString("online_training");
                            OfflineStatus = dataObject.getString("offline_training");
                            VirtualStatus = dataObject.getString("virtual_training");

                            Log.d(TAG, response.toString() + "--- Add responce  ----" + response);
                        } catch (Exception e) {
                            hideDialog();
                            Log.d(TAG, response.toString() + "--- Add responce  ----" + e.getMessage());
                        }
                        hideDialog();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d(TAG, error.getMessage() + "responce");

                        hideDialog();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            String message = data.optString("message");

                            Toast.makeText(WorkoutActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + SharedpreferenceUtility.getInstance(WorkoutActivity.this).getUser(SAVE_TOKEN));
                Log.e("response", "Data " + params.toString());
                return params;

            }
        };

        Mysingleton.getInstance(WorkoutActivity.this).addTorequestque(jsonObjReq);
    }

}
