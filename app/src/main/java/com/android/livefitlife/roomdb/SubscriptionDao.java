package com.android.livefitlife.roomdb;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SubscriptionDao {

    @Query("SELECT * FROM SubscriptionModelRoom")
    List<SubscriptionModelRoom> getAllData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SubscriptionModelRoom task);

    @Delete
    void delete(SubscriptionModelRoom task);

    @Query("SELECT status FROM SubscriptionModelRoom WHERE actiId = :id")
    String getStatusOfCategory(String id);

    @Query("UPDATE SubscriptionModelRoom SET status=:status WHERE actiId = :id")
    void updateStatusOfCategory(String status, String id);

    @Update
    void update(SubscriptionModelRoom task);


}
