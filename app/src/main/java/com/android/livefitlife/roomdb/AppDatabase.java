package com.android.livefitlife.roomdb;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {SubscriptionModelRoom.class}, version = 1)
    public abstract class AppDatabase extends RoomDatabase {
        public abstract SubscriptionDao taskDao();
    }
