package com.android.livefitlife.roomdb;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;

public class UtilClass {

    public static void insertData(final Context context, final ArrayList<SubscriptionModelRoom> subs) {
        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                for (int i = 0; i < subs.size(); i++) {
                    SubscriptionModelRoom subscriptionModel = subs.get(i);

                    DatabaseClient.getInstance(context).getAppDatabase()
                            .taskDao()
                            .insert(subscriptionModel);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(context, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }


    public static void updateActivityStatus(final Context context, String status, String Activityid) {
        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task

                DatabaseClient.getInstance(context).getAppDatabase()
                        .taskDao()
                        .updateStatusOfCategory(status, Activityid);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(context, "Updated Data", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }

}
