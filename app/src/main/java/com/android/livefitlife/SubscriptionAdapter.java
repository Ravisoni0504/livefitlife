package com.android.livefitlife;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.livefitlife.Utility.OnClickBuyPlan;
import com.android.livefitlife.Utility.OnClickDietPlan;

import java.util.List;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.MyViewHolder> {

    List<SubscriptionModel> videoListModels;
    Context context;
    OnClickBuyPlan onClickBuyPlan;

    public SubscriptionAdapter(Context context, List<SubscriptionModel> videoListModels, OnClickBuyPlan onClickBuyPlan) {
        this.videoListModels = videoListModels;
        this.onClickBuyPlan = onClickBuyPlan;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_model, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final SubscriptionModel professionalListModel = videoListModels.get(position);


        if (professionalListModel.getDietType().equals("NA") && professionalListModel.getClientType().equals("NA")) {
            holder.planName.setText(professionalListModel.getDayswithDiet());
            holder.priceWithDiet.setText(professionalListModel.getPricewithDiet());
            holder.descWithDiet.setText(professionalListModel.getDescwithDiet());
            holder.priceWithoutDiet.setVisibility(View.GONE);
            holder.descWithoutDiet.setVisibility(View.GONE);
            holder.buy2.setVisibility(View.GONE);
            holder.view.setVisibility(View.GONE);
        } else {
            holder.planName.setText(professionalListModel.getDayswithDiet());
            holder.priceWithDiet.setText(professionalListModel.getPricewithDiet());
            holder.descWithDiet.setText(professionalListModel.getDescwithDiet());
            holder.priceWithoutDiet.setText(professionalListModel.getPricewithoutDiet());
            holder.descWithoutDiet.setText(professionalListModel.getDescwithoutDiet());
            holder.priceWithoutDiet.setVisibility(View.VISIBLE);
            holder.descWithoutDiet.setVisibility(View.VISIBLE);
            holder.buy2.setVisibility(View.VISIBLE);
            holder.view.setVisibility(View.VISIBLE);

        }


        //holder.discount.setText(professionalListModel.getDetailTxt());

        holder.buy1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBuyPlan.OnClickWithDiet(position);
                //   Intent intent = new Intent(context, MovieDetailActivity.class);
                //              context.startActivity(intent);
            }
        });

        holder.buy2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBuyPlan.OnClickWithoutDiet(position);
                //   Intent intent = new Intent(context, MovieDetailActivity.class);
                //              context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoListModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView planName, days, priceWithDiet, priceWithoutDiet, descWithDiet, descWithoutDiet, view;
        Button buy1, buy2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            planName = itemView.findViewById(R.id.planName);
            priceWithDiet = itemView.findViewById(R.id.priceWithDiet);
            priceWithoutDiet = itemView.findViewById(R.id.priceWithoutDiet);
            descWithDiet = itemView.findViewById(R.id.descWithDiet);
            descWithoutDiet = itemView.findViewById(R.id.descWithoutDiet);
            view = itemView.findViewById(R.id.view);
            buy1 = itemView.findViewById(R.id.buyWithDiet);
            buy2 = itemView.findViewById(R.id.buyWithoutDiet);


        }
    }

}
