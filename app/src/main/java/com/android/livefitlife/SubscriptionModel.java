package com.android.livefitlife;

public class SubscriptionModel {
    String headerTxt;
    String dayswithDiet;
    String PricewithDiet;
    String DescwithDiet;
    String PriceOnlywithDiet;

    public String getPriceOnlywithDiet() {
        return PriceOnlywithDiet;
    }

    public void setPriceOnlywithDiet(String priceOnlywithDiet) {
        PriceOnlywithDiet = priceOnlywithDiet;
    }

    public String getPriceOnlywithoutDiet() {
        return PriceOnlywithoutDiet;
    }

    public void setPriceOnlywithoutDiet(String priceOnlywithoutDiet) {
        PriceOnlywithoutDiet = priceOnlywithoutDiet;
    }

    String PriceOnlywithoutDiet;

    public String getDescwithDiet() {
        return DescwithDiet;
    }

    public void setDescwithDiet(String descwithDiet) {
        DescwithDiet = descwithDiet;
    }

    public String getDescwithoutDiet() {
        return DescwithoutDiet;
    }

    public void setDescwithoutDiet(String descwithoutDiet) {
        DescwithoutDiet = descwithoutDiet;
    }

    String DescwithoutDiet;

    public String getDayswithDiet() {
        return dayswithDiet;
    }

    public void setDayswithDiet(String dayswithDiet) {
        this.dayswithDiet = dayswithDiet;
    }

    public String getPricewithDiet() {
        return PricewithDiet;
    }

    public void setPricewithDiet(String pricewithDiet) {
        PricewithDiet = pricewithDiet;
    }

    public String getDietType() {
        return dietType;
    }

    public void setDietType(String dietType) {
        this.dietType = dietType;
    }

    public String getDayswithoutDiet() {
        return dayswithoutDiet;
    }

    public void setDayswithoutDiet(String dayswithoutDiet) {
        this.dayswithoutDiet = dayswithoutDiet;
    }

    public String getPricewithoutDiet() {
        return PricewithoutDiet;
    }

    public void setPricewithoutDiet(String pricewithoutDiet) {
        PricewithoutDiet = pricewithoutDiet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String dietType;
    String dayswithoutDiet;
    String PricewithoutDiet;

    public String getDescclientTypeDiet() {
        return DescclientTypeDiet;
    }

    public void setDescclientTypeDiet(String descclientTypeDiet) {
        DescclientTypeDiet = descclientTypeDiet;
    }

    String DescclientTypeDiet;

    public String getDaysclientTypeDiet() {
        return daysclientTypeDiet;
    }

    public void setDaysclientTypeDiet(String daysclientTypeDiet) {
        this.daysclientTypeDiet = daysclientTypeDiet;
    }

    public String getPriceclientTypeDiet() {
        return PriceclientTypeDiet;
    }

    public void setPriceclientTypeDiet(String priceclientTypeDiet) {
        PriceclientTypeDiet = priceclientTypeDiet;
    }

    String daysclientTypeDiet;
    String PriceclientTypeDiet;
    String id;

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    String clientType;


    public String getHeaderTxt() {
        return headerTxt;
    }

    public void setHeaderTxt(String headerTxt) {
        this.headerTxt = headerTxt;
    }


}
