package com.android.livefitlife.Utility;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.livefitlife.R;

public class BaseActivity extends AppCompatActivity {
    CustomProgressBar customProgress = CustomProgressBar.getInstance();

// now you have the instance of CustomProgres
// for showing the ProgressBar


// for hiding the ProgressBar


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }

    public void showProgressDialog() {
        customProgress.showProgress(BaseActivity.this, "Please Wait", true);
    }

    public void hideDialog() {
        customProgress.hideProgress();
    }
}
