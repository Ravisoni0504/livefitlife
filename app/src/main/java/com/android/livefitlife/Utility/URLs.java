package com.android.livefitlife.Utility;

public class URLs {

    //public static String BASE_URL = "http://aarogyaindia.in/";
    public static String BASE_URL = "http://livefitlife.in/";
    public static String LOGIN_URL = BASE_URL + "admin/public/api/v1/login";
    public static String SEND_OTP = BASE_URL + "admin/public/api/v1/sendotp";
    public static String GET_LEVEL_LIST = BASE_URL + "admin/public/api/v1/levels/getLevelList";
    public static String REGISTER = BASE_URL + "admin/public/api/v1/register";
    public static String ACTIVITIES_URL = BASE_URL + "admin/public/api/v1/getCategoryList";
    public static String SOCIAL_CHECK = BASE_URL + "admin/public/api/v1/socialcheck";
    public static String GET_PLAN = BASE_URL + "admin/public/api/v1/plans/getPlanList";
    public static String GENERATE_ORDER = BASE_URL + "admin/public/api/v1/orders/generateOrder";
    public static String VERIFY_PAYMENT = BASE_URL + "admin/public/api/v1/orders/verifyPayment";
    public static String GET_TRAINER_BY_ID = BASE_URL + "admin/public/api/v1/users/getTrainerByCategory";
    public static String DEMO_REQUEST = BASE_URL + "admin/public/api/v1/demorequests/addDemoRequest";
    public static String TEST_REQUEST = BASE_URL + "admin/public/api/v1/testrequests/addTestRequest";
    public static String GET_PROFILE = BASE_URL + "admin/public/api/v1/users/getMyProfile";
    public static String EDIT_PROFILE = BASE_URL + "admin/public/api/v1/users/editMyProfile";

    //admin/public/api/v1/demorequests/getDemoRequestStatus
    public static String DEMO_TEST_STATUS = BASE_URL + "admin/public/api/v1/testrequests/addTestRequest";
    public static String DEMO_TEST = BASE_URL + "admin/public/api/v1/testrequests/addTestRequest";
    public static String GET_DIET_LIST = BASE_URL + "admin/public/api/v1/diets/getDietList";
    public static String GET_WORKOUT_VIDEOS = BASE_URL + "admin/public/api/v1/levels/getLevelList";
    public static String GET_WARMUP_LIST = BASE_URL + "admin/public/api/v1/getWarmupList";
    public static String GET_USER_SETTINGS = BASE_URL + "admin/public/api/v1/users/getSettings";
    public static String GET_PLANBY_CATEGORY = BASE_URL + "admin/public/api/v1/plans/getPlanByCategory";
    public static String POST_REQUEST_DEMO = BASE_URL + "admin/public/api/v1/demorequests/addDemoRequest";
    public static String GET_LEVEL_DATA = BASE_URL + "admin/public/api/v1/levels/getLevelById";

    public static String RECEIPE_LIST = BASE_URL + "admin/public/api/v1/recipes/getRecipeList";
    public static String SLOT_GET = BASE_URL + "admin/public/api/v1/slots/getSlot";
    public static String BOOK_SLOT = BASE_URL + "admin/public/api/v1/bookings/book";

    public static String GET_SUBSCRIPTION = BASE_URL + "admin/public/api/v1/orders/getsubscription";
    public static String GET_REQUEST_DEMO_STATUS = BASE_URL + "admin/public/api/v1/demorequests/getDemoRequestStatus";


    public static String GET_SCHEDULE_TDY = BASE_URL + "admin/public/api/v1/schedules/getMyScheduleToday";
    public static String GET_NOTIFICATION = BASE_URL + "admin/public/api/v1/users/getMyNotifications";
    public static String CONTACT_US = BASE_URL + "admin/public/api/v1/contactus";
    public static String GET_ACCESS_TOKEN = BASE_URL + "admin/public/api/v1/getAccessToken";
   // public static String GET_LEVEL_LIST = BASE_URL + "gym/public/api/v1/levels/getLevelList";


}
