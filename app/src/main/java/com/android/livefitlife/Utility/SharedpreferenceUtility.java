package com.android.livefitlife.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedpreferenceUtility {


    public static SharedPreferences mPref;
    private static SharedpreferenceUtility mRef;
    private SharedPreferences.Editor mEditor;

    public static String SAVE_ID = "_id";
    public static String SAVE_USER_ID = "user_id";
    public static String SAVE_NAME = "user_name";
    public static String SAVE_EMAIL = "user_email";
    public static String SAVE_IMAGE = "user_image";
    public static String SAVE_AGE = "user_age";
    public static String SAVE_SUBSCRIPTION_STATUS = "save_subscription";
    public static String SAVE_PHONE = "user_phone";
    public static String SAVE_TOKEN = "user_token";
    public static String SAVE_GENDER = "user_gender";
    public static String SAVE_WEIGHT = "user_weight";
    public static String SAVE_HEIGHT = "user_height";
    public static String CHECKPERMISSION = "check_permission";
    public static String LOCAL_TABLE_STATUS = "false";

    /**
     * Singleton method return the instance
     **/
    public static SharedpreferenceUtility getInstance(Context context) {
        if (mRef == null) {
            mRef = new SharedpreferenceUtility();
            mPref = context.getApplicationContext().getSharedPreferences(
                    "MyPref", 0);
            return mRef;
        }
        return mRef;
    }


    //Put long value into sharedpreference

    public void setUser(String key, String value) {
        try {
            mEditor = mPref.edit();
            mEditor.putString(key, value);
            mEditor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Get long value from sharedpreference

    public String getUser(String key) {
        String lvalue = "";
        try {
            lvalue = mPref.getString(key, "0");
        } catch (Exception e) {
            e.printStackTrace();
            lvalue = "0";
        }
        return lvalue;
    }

}
